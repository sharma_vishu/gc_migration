#!/bin/bash
###############################################################################
#                               Documentation                                 #
###############################################################################
#                                                                             #
# Description                                                                 #
#     :
#                                                                             #
#                                                                             #
#                                                                             #
###############################################################################
#                           Identify Script Home                              #
###############################################################################
#Find the script file home
pushd . > /dev/null
SCRIPT_DIRECTORY="${BASH_SOURCE[0]}";
while([ -h "${SCRIPT_DIRECTORY}" ]);
do
  cd "`dirname "${SCRIPT_DIRECTORY}"`"
  SCRIPT_DIRECTORY="$(readlink "`basename "${SCRIPT_DIRECTORY}"`")";
done
cd "`dirname "${SCRIPT_DIRECTORY}"`" > /dev/null
SCRIPT_DIRECTORY="`pwd`";
popd  > /dev/null
MODULE_HOME="`dirname "${SCRIPT_DIRECTORY}"`"
###############################################################################
#                           Import Dependencies                               #
###############################################################################

#Load common dependencies
. ${MODULE_HOME}/bin/import-dependecies.sh

###############################################################################
#                                CODE                                         #
###############################################################################
APPLICATION_INSTALL_DIR='/home/nzhdusr/tkmah9c/current'
echo "I am in validation step"

MODULE_CLASSPATH=""
JAR_PATH=${SHARED_LIB}/utility.jar
AIRBNB_JAR_PATH=${APPLICATION_INSTALL_DIR}/of-sync-utility/etc/airbnb-reair-main-1.0.0.jar
chmod 777 ${AIRBNB_JAR_PATH}

while getopts ":t:f:" opt; do
  case $opt in
    t) SYNC_TABLE="$OPTARG"
    ;;
    f) FLOW_NAME="$OPTARG"
    ;;
    *) echo "Invalid option -$OPTARG"
    ;;
  esac
done

CONFIG_FILE_PATH=${CONFIG_HOME}/${FLOW_NAME}"-"${SYNC_TABLE}"/sync-configurations.xml"
TEXT_FILE_PATH=${SYNC_TABLE}"_text"

#Update the status of the job
java -cp ${SHARED_LIB}/utility.jar com.utility.migrator.sql_utility.JobTaskUpdater -f ${FLOW_NAME} -t ${SYNC_TABLE} -jst reair-sync-utility -jss RUNNING -c ${CONFIG_FILE_PATH}

#Creating table list in HDFS
PATH_NAME=/migrator/${FLOW_NAME}'-'${SYNC_TABLE}/
FILE_NAME=${PATH_NAME}${SYNC_TABLE}".txt"
hadoop fs -rmr ${PATH_NAME}
hadoop fs -mkdir ${PATH_NAME}
echo ${SYNC_TABLE} | hadoop fs -put - ${FILE_NAME}

hadoop jar ${AIRBNB_JAR_PATH} com.airbnb.reair.batch.hive.MetastoreReplicationJob \
-D dfs.ha.namenodes.KOHLSBIGDATA2NNHA=nn1,nn2 -D dfs.namenode.rpc-address.KOHLSBIGDATA2NNHA.nn1=10.200.32.132:8020 \
-D dfs.namenode.rpc-address.KOHLSBIGDATA2NNHA.nn2=10.200.32.132:8020 -D dfs.nameservices=KOHLSBIGDATA2NNHA,ddh-dev-dataproc \
-D dfs.client.failover.proxy.provider.KOHLSBIGDATA2NNHA=org.apache.hadoop.hdfs.server.namenode.ha.ConfiguredFailoverProxyProvider \
-D dfs.ha.namenodes.ddh-dev-dataproc=nn1,nn2 -D dfs.namenode.rpc-address.ddh-dev-dataproc.nn1=ddh-dev-dataproc-m-1:8020 \
-D dfs.namenode.rpc-address.ddh-dev-dataproc.nn2=ddh-dev-dataproc-m-0:8020 \
-D dfs.client.failover.proxy.provider.ddh-dev-dataproc=org.apache.hadoop.hdfs.server.namenode.ha.ConfiguredFailoverProxyProvider \
--config-files ${CONFIG_FILE_PATH} --table-list ${FILE_NAME}

exit_code=`fn_get_exit_code $?`
fn_handle_exit_code_modules "${exit_code}" "Meta Data Sync Successfull" "Failed to sync the metadata" "${FAIL_ON_ERROR}" "${FLOW_NAME}" "${SYNC_TABLE}"

#Destination Table Location Correction and MSCK REPAIR TABLE UPDATOR
java -cp ${SHARED_LIB}/utility.jar com.utility.migrator.sync_utility.LocationCorrector -t ${SYNC_TABLE} -c ${CONFIG_FILE_PATH}

REGEX_LOCATION_STRING=$(echo $SYNC_TABLE | sed 's/\./\//')
#Creation of AVRO Schema File in HDFS
TEXT_TBL_LOCATION=`hive -S -e "describe formatted  ${SYNC_TABLE};" | grep 'Location' | grep 'gs://' | awk '{ print $NF }' | tail -n 1`
AVRO_TBL_LOCATION=${TEXT_TBL_LOCATION}"_text_avro"
hadoop fs -rm -r ${AVRO_TBL_LOCATION}

AVRO_SCHEMA_FILE_PATH=${AVRO_TBL_LOCATION}"/avro_schema.avsc"
AVRO_SCHEMA=$(java -cp ${SHARED_LIB}/utility.jar com.utility.migrator.sync_utility.CreateAvroSchema -t ${SYNC_TABLE} -c ${CONFIG_FILE_PATH})
echo ${AVRO_SCHEMA} | hadoop fs -put - ${AVRO_SCHEMA_FILE_PATH}

#Incremental Hive Table Automator
java -cp ${SHARED_LIB}/utility.jar com.utility.migrator.sync_utility.IncrementalHiveTableAutomator -f ${FLOW_NAME} -t ${SYNC_TABLE} -c ${CONFIG_FILE_PATH}
exit_code=`fn_get_exit_code $?`
fn_handle_exit_code_modules "${exit_code}" "Incremental Hive Table Stage completed successfully" "Failed to incrementally load data" "${FAIL_ON_ERROR}" "${FLOW_NAME}" "${SYNC_TABLE}"

#Text Table Automator
java -cp ${SHARED_LIB}/utility.jar com.utility.migrator.sync_utility.TextTableLoader -f ${FLOW_NAME} -t ${SYNC_TABLE} -tfp ${APPLICATION_INSTALL_DIR}/of-sync-utility/etc/schema/TextTableTemplate.vm -c ${CONFIG_FILE_PATH} -afp ${AVRO_SCHEMA_FILE_PATH}

exit_code=`fn_get_exit_code $?`
fn_handle_exit_code_modules "${exit_code}" "Text Hive Table Stage completed successfully" "Failed to create text table or load" "${FAIL_ON_ERROR}" "${FLOW_NAME}" "${SYNC_TABLE}"

echo "Successfully completed all subsequent jobs to completely load the data in the text table"

#Update the status of the job
java -cp ${SHARED_LIB}/utility.jar com.utility.migrator.sql_utility.JobTaskUpdater -f ${FLOW_NAME} -t ${SYNC_TABLE} -jst text-table-loader-utility -jss RUNNING -c ${CONFIG_FILE_PATH}

exit_code=`fn_get_exit_code $?`
fn_handle_exit_code_modules "${exit_code}" "Successfully completed the sync utility" "Failed" "${FAIL_ON_ERROR}" "${FLOW_NAME}" "${SYNC_TABLE}"

#Renaming the Table Partitions in case of Incremental loading
java -cp ${SHARED_LIB}/utility.jar com.utility.migrator.sync_utility.RenameTextTablePartitions -f ${FLOW_NAME} -t ${SYNC_TABLE} -c ${CONFIG_FILE_PATH}

exit_code=`fn_get_exit_code $?`
fn_handle_exit_code_modules "${exit_code}" "Successfully completed renaming the Partition files" "Failed to complete renaming the partition files" "${FAIL_ON_ERROR}" "${FLOW_NAME}" "${SYNC_TABLE}"

#Update the status of the job
java -cp ${SHARED_LIB}/utility.jar com.utility.migrator.sql_utility.JobTaskUpdater -f ${FLOW_NAME} -t ${SYNC_TABLE} -jst text-table-rename-partitions -jss RUNNING -c ${CONFIG_FILE_PATH}


###############################################################################
#                                     End                                     #
###############################################################################