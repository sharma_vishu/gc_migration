--###############################################################################
--#                               Documentation                                 #
--###############################################################################
--#                                                                             #
--# Description                                                                 #
--#     :                                                                       #
--#                                                                             #
--#                                                                             #
--#                                                                             #
--###############################################################################
--#                                DDL Statement                                #
--###############################################################################

dfs -rm -r ${REAIR_OP_DIR}/step2output;

DROP TABLE IF EXISTS DB_NAME.replication_step2_output;

CREATE TABLE IF NOT EXISTS DB_NAME.replication_step2_output (
      action_name STRING,
      src_path STRING,
      dest_path STRING,
      size STRING,
      extra STRING,
      action_time STRING
      )
PARTITIONED BY (
      job_start_time STRING
      )
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE;


--################################################################################
--#                                     End                                      #
--################################################################################