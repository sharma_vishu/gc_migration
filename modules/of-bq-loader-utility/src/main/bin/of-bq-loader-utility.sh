#!/bin/bash
###############################################################################
#                               Documentation                                 #
###############################################################################
#                                                                             #
# Description                                                                 #
#     :
#                                                                             #
#                                                                             #
#                                                                             #
###############################################################################
#                           Identify Script Home                              #
###############################################################################
#Find the script file home
pushd . > /dev/null
SCRIPT_DIRECTORY="${BASH_SOURCE[0]}";
while([ -h "${SCRIPT_DIRECTORY}" ]);
do
  cd "`dirname "${SCRIPT_DIRECTORY}"`"
  SCRIPT_DIRECTORY="$(readlink "`basename "${SCRIPT_DIRECTORY}"`")";
done
cd "`dirname "${SCRIPT_DIRECTORY}"`" > /dev/null
SCRIPT_DIRECTORY="`pwd`";
popd  > /dev/null
MODULE_HOME="`dirname "${SCRIPT_DIRECTORY}"`"
###############################################################################
#                           Import Dependencies                               #
###############################################################################
#Load common dependencies
. ${MODULE_HOME}/bin/import-dependecies.sh
###############################################################################
#                                CODE                                         #
###############################################################################
while getopts ":t:f:" opt; do
  case $opt in
    t) SYNC_TABLE="$OPTARG"
    ;;
    f) FLOW_NAME="$OPTARG"
    ;;
    *) echo "Invalid option -$OPTARG"
    ;;
  esac
done
###############################################################################

  CONFIG_FILE_PATH=${CONFIG_HOME}/${FLOW_NAME}"-"${SYNC_TABLE}"/sync-configurations.xml"
  #BQ_FILE_PATH=${CONFIG_HOME}/${FLOW_NAME}"-"${SYNC_TABLE}"/"${SYNC_TABLE}".json"
  BQ_DATASET=$(java -cp ${SHARED_LIB}/utility.jar com.utility.migrator.loader_utility.FetchBQName -t ${SYNC_TABLE} -c ${CONFIG_FILE_PATH})
  BQ_STAGE_TABLE=${BQ_DATASET}"_stage"
  HIVE_TEXT_TABLE_NAME=${SYNC_TABLE}"_text"

  #Creating BQ Schema from Persisted Schema Table
  #select bigquery_schema from schema_details where table_name=${SYNC_TABLE}
  #BQ_SCHEMA_STRING=$(java -cp ${SHARED_LIB}/utility.jar com.utility.migrator.loader_utility.FetchBQSchema "${SYNC_TABLE}" "${CONFIG_FILE_PATH}")
  #echo ${BQ_SCHEMA_STRING} | tee ${BQ_FILE_PATH}

  declare -a partition_error_record=();
  #Code to check if the Avro based Hive Table exists
  hive -e "describe ${HIVE_TEXT_TABLE_NAME}" >> /dev/null
  err_code=`echo "$?"`
  if [ $err_code -ne 0 ]
    then
      echo "Text Formatted Hive Table doesn't exists ! "
      exit 1
  fi
  # Deleting the staging Big Query Table, if it exists
  # Recreating the Same BQ Table

   #bq rm -f $BQ_STAGE_TABLE
   #bq mk --time_partitioning_type=DAY $BQ_STAGE_TABLE

   #err_code=`echo "$?"`
   #if [ ${err_code} -eq 0 ]
   #then
   #     echo "staging stable created"
   #else
   #     echo "failed to create staging table"
   #     exit 1
   #fi

  echo "BQ_DATASET=${BQ_DATASET}"
  # Creating back-up table of the principal bigquery table
   bq show ${BQ_DATASET} >>/dev/null
   if [ $? -eq 0 ]
    then
      bq rm -f ${BQ_DATASET}_backup
      bq cp ${BQ_DATASET} ${BQ_DATASET}_backup
      if [ $? -ne 0 ]
       then
           echo "Big Query backup operation failed ...."
           exit 1
      fi
  fi

  bq mk --time_partitioning_type=DAY ${BQ_DATASET}_stage_partition

  partition_array=(`hive -S -e "show partitions ${HIVE_TEXT_TABLE_NAME};"`)
  gs_location=`hive -S -e "describe formatted  ${HIVE_TEXT_TABLE_NAME};" | grep 'Location'| awk '{ print $NF }'`
  echo "Google Storage Location for the text table Loader file is ${gs_location}"

ingestion_time=`date +%Y%m%d`
if [ ${#partition_array[@]} -eq 1 ] && [ "$partition_id" == "$ingestion_time" ]
  then
  echo "non-partitioned table "
   partition_id=`echo ${PARTITIONS} | awk -F '=' '{ print $2 }'`
   bq cp --append_table ${BQ_DATASET}  ${BQ_DATASET}_stage_partition
   bq rm -f ${BQ_DATASET}
else
  echo "partitioned table"
  for  PARTITIONS in "${partition_array[@]}"
   do
    partition_id=`echo ${PARTITIONS} | awk -F '=' '{ print $2 }'`
   QUERY=${BQ_DATASET}'$'${partition_id}
   QUERY_STAGE=${BQ_DATASET}_stage_partition'$'${partition_id}
   bq query --allow_large_results --append_table --noflatten_results --destination_table ${QUERY_STAGE} 'select * from ${QUERY}'>>/dev/null
   exit_value=`echo "$?"`
   if [ exit_value -ne 0 ]
    then
    echo "failed to copy modified partitions into $BQ_DATASET_stage_partition !!!!"
    exit 1
  else
    bq rm -f ${QUERY}
  fi
  done
fi

      for PARTITIONS in "${partition_array[@]}"
    do
      partition_id=`echo ${PARTITIONS} | awk -F '=' '{ print $2 }'`
      partition_column=`echo ${PARTITIONS} | awk -F '=' '{ print $1 }'`
      echo "Loading into staging table : ${partition_id} = ${partition_column}"
      echo "Text file being loaded : ${gs_location}/$PARTITIONS"
      #filename=(`hadoop fs -ls $gs_location/$PARTITIONS/ | awk  -F ' ' '{print $8}' | tail -n +2 `)
      PART_FILES=(`gsutil ls $gs_location/$PARTITIONS/ | tail -n +2 `)
      echo $PART_FILES
      for PART_FILE in "${PART_FILES[@]}"
      do
         QUERY=${BQ_STAGE_TABLE}'$'${partition_id}
         echo "query=${QUERY}"
         #bq load --source_format=AVRO -F '\t' $QUERY ${PART_FILE}
         bq load --source_format=AVRO $QUERY ${PART_FILE}
         exit_code=`echo "$?"`
         if [ $exit_code -ne 0 ]
         then
             partition_error_record=("${partition_error_record[@]}" $PART_FILE)
         fi
      done
  done

  #End of loading text file partitions into staging table
   #Loading data from staging big-query to final big-query table.
   bq cp --append_table ${BQ_STAGE_TABLE} ${BQ_DATASET}
   error_code=`echo "$?"`

  #Creating Audit of the failed partitions into mysql-audit-table.
   ERROR_PARTITIONS_LENGTH=${#partition_error_record[@]}
  if [ ${ERROR_PARTITIONS_LENGTH} -ne 0 ]
  then
    echo "Started to persist the failed partition details ......"
    for ERROR_PARTITIONS in "${partition_error_record[@]}"
    do
       java -cp ${SHARED_LIB}/utility.jar com.utility.migrator.loader_utility.PersistFailedPartitions -f ${FLOW_NAME} -t ${HIVE_TEXT_TABLE_NAME} -p ${ERROR_PARTITIONS} -c ${CONFIG_FILE_PATH}
    done
  else
    echo "No failed partitions found, nothing to persist."
  fi

  #deleting backup and staging bq table
  if [ $error_code -eq 0 ]
    then
      bq rm -f ${BQ_STAGE_TABLE}
      bq rm -f ${BQ_DATASET}_backup
      bq rm -f ${BQ_DATASET}_stage_partition
  else
       exit 1
  fi

###############################################################################
#                                     End                                     #
###############################################################################