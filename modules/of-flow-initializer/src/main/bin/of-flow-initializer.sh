#!/bin/bash
###############################################################################
#                               Documentation                                 #
###############################################################################
#                                                                             #
# Description                                                                 #
#     :
#                                                                             #
#                                                                             #
#                                                                             #
###############################################################################
#                           Identify Script Home                              #
###############################################################################
#Find the script file home
pushd . > /dev/null
SCRIPT_DIRECTORY="${BASH_SOURCE[0]}";
while([ -h "${SCRIPT_DIRECTORY}" ]);
do
  cd "`dirname "${SCRIPT_DIRECTORY}"`"
  SCRIPT_DIRECTORY="$(readlink "`basename "${SCRIPT_DIRECTORY}"`")";
done
cd "`dirname "${SCRIPT_DIRECTORY}"`" > /dev/null
SCRIPT_DIRECTORY="`pwd`";
popd  > /dev/null
MODULE_HOME="`dirname "${SCRIPT_DIRECTORY}"`"
###############################################################################
#                           Import Dependencies                               #
###############################################################################

#Load common dependencies
. ${MODULE_HOME}/bin/import-dependecies.sh

###############################################################################
#                                CODE                                         #
###############################################################################


while getopts ":t:f:" opt; do
  case $opt in
    t) SYNC_TABLE="$OPTARG"
    ;;
    f) FLOW_NAME="$OPTARG"
    ;;
    *) echo "Invalid option -$OPTARG"
    ;;
  esac
done

MODULE_CLASSPATH=""
JAR_PATH=${SHARED_LIB}/utility.jar

  for i in $module_home/lib/*.jar; do
    MODULE_CLASSPATH=$MODULE_CLASSPATH:$i
  done


  SUCCESS_MESSAGE='Successfully completed executing Flow Registration.'
  FAILURE_MESSAGE='Failed to complete flow registration'
  VT_CONF_FILE=${APPLICATION_INSTALL_DIR}/of-flow-initializer/etc/reair-configurations.vm
  CONFIG_FILE_PATH=${CONFIG_HOME}/${FLOW_NAME}"-"${SYNC_TABLE}"/sync-configurations.xml"
  CONFIG_PROPERTIES_FILE=${APPLICATION_INSTALL_DIR}/config/reair-configurations.properties

  echo $VT_CONF_FILE
  echo $CONFIG_FILE_PATH

  #Destination Cluster Setup for First time load

  #Automated Configuration File Generator
  java -cp ${JAR_PATH} com.utility.migrator.initializer.AutomatedConfigGenerator -cf ${CONFIG_PROPERTIES_FILE} -vm ${VT_CONF_FILE} -t ${SYNC_TABLE} -c ${CONFIG_HOME} -f ${FLOW_NAME}

  #Validating if this particular flow isn't already runnning, if not - registering this as a new flow
  java -cp ${JAR_PATH} com.utility.migrator.initializer.JobInitializater -f ${FLOW_NAME} -t ${SYNC_TABLE} -c ${CONFIG_FILE_PATH}

  exit_code=`fn_get_exit_code $?`
  fn_handle_exit_code_modules "${exit_code}" "${SUCCESS_MESSAGE}" "${FAILURE_MESSAGE}" "${FAIL_ON_ERROR}" "${FLOW_NAME}" "${SYNC_TABLE}"

###############################################################################
#                                     End                                     #
###############################################################################
