#!/bin/bash
###############################################################################
#                               Documentation                                 #
###############################################################################
#                                                                             #
# Description                                                                 #
#     :
#                                                                             #
#                                                                             #
#                                                                             #
###############################################################################
#                           Identify Script Home                              #
###############################################################################
#Find the script file home
pushd . > /dev/null
SCRIPT_DIRECTORY="${BASH_SOURCE[0]}";
while([ -h "${SCRIPT_DIRECTORY}" ]);
do
  cd "`dirname "${SCRIPT_DIRECTORY}"`"
  SCRIPT_DIRECTORY="$(readlink "`basename "${SCRIPT_DIRECTORY}"`")";
done
cd "`dirname "${SCRIPT_DIRECTORY}"`" > /dev/null
SCRIPT_DIRECTORY="`pwd`";
popd  > /dev/null
MODULE_HOME="`dirname "${SCRIPT_DIRECTORY}"`"
###############################################################################
#                           Import Dependencies                               #
###############################################################################
#Load common dependencies
. ${MODULE_HOME}/bin/import-dependecies.sh

###############################################################################
#                                CODE                                         #
###############################################################################

echo "I am in Trigger utility shell script"
CONFIG_HOME=${APPLICATION_INSTALL_DIR}/config
SHARED_LIB=${APPLICATION_INSTALL_DIR}/lib

MODULE_CLASSPATH=""

JAR_PATH=${SHARED_LIB}/utility.jar

TRIGGER_CONFIG_FILE_PATH=${CONFIG_HOME}"/trigger-configurations.properties"

#Trigger Framework Starter Job
java -cp ${JAR_PATH} com.trigger.utilities.TriggerMain -c ${TRIGGER_CONFIG_FILE_PATH}

exit_code=`fn_get_exit_code $?`
fn_handle_exit_code_modules "${exit_code}" "Successfully completed the sync utility" "Failed" "${FAIL_ON_ERROR}" "${FLOW_NAME}" "${SYNC_TABLE}"

###############################################################################
#                                     End                                     #
###############################################################################
