#!/bin/bash
###############################################################################
#                               Documentation                                 #
###############################################################################
#                                                                             #
# Description                                                                 #
#     :
#                                                                             #
#                                                                             #
#                                                                             #
###############################################################################
#                           Identify Script Home                              #
###############################################################################
#Find the script file home
pushd . > /dev/null
SCRIPT_DIRECTORY="${BASH_SOURCE[0]}";
while([ -h "${SCRIPT_DIRECTORY}" ]);
do
  cd "`dirname "${SCRIPT_DIRECTORY}"`"
  SCRIPT_DIRECTORY="$(readlink "`basename "${SCRIPT_DIRECTORY}"`")";
done
cd "`dirname "${SCRIPT_DIRECTORY}"`" > /dev/null
SCRIPT_DIRECTORY="`pwd`";
popd  > /dev/null
MODULE_HOME="`dirname "${SCRIPT_DIRECTORY}"`"
###############################################################################
#                           Import Dependencies                               #
###############################################################################

#Load common dependencies
. ${MODULE_HOME}/bin/import-dependecies.sh

###############################################################################
#                                CODE                                         #
###############################################################################

while getopts ":t:f:" opt; do
  case $opt in
    t) SYNC_TABLE="$OPTARG"
    ;;
    f) FLOW_NAME="$OPTARG"
    ;;
    *) echo "Invalid option -$OPTARG"
    ;;
  esac
done
  JAR_PATH=${SHARED_LIB}/utility.jar
  CONFIG_FILE_PATH=${CONFIG_HOME}/${FLOW_NAME}"-"${SYNC_TABLE}"/sync-configurations.xml"
  HIVE_TEXT_TABLE_NAME=${SYNC_TABLE}"_text"

  S3_BUCKET_NAME=$(java -cp ${JAR_PATH} com.utility.migrator.s3_utility.GetS3BucketName -t ${SYNC_TABLE} -c ${CONFIG_FILE_PATH})

  echo "S3 Bucket Name is = ${S3_BUCKET_NAME}"

  S3_COPY_PATH=${S3_BUCKET_NAME}"/"${SYNC_TABLE}

  declare -a partition_error_record=();

  #Code to check if the Avro based Hive Table exists
  hive -e "describe ${HIVE_TEXT_TABLE_NAME}" >> /dev/null
  err_code=`echo "$?"`

  if [ $err_code -ne 0 ]
    then
      echo "Text Formatted Hive Table doesn't exists ! "
      exit 1
  fi

  GS_TABLE_LOCATION=`hive -S -e "describe formatted  ${HIVE_TEXT_TABLE_NAME};" | grep 'Location'| awk '{ print $NF }'`

  echo "Google Storage Location for the text table Loader file is ${GS_TABLE_LOCATION}"

  #Migrate from the Partition Start into Amazon S3
  # gsutil -m cp -R ${GS_TABLE_LOCATION} ${S3_COPY_PATH}

  #Instead of a simple migration, we have decided to sync the complete subdirectory
  gsutil -m rsync -r ${GS_TABLE_LOCATION} ${S3_COPY_PATH}

###############################################################################
#                                     End                                     #
###############################################################################
