#!/bin/bash
###############################################################################
#                               Documentation                                 #
###############################################################################
#                                                                             #
# Description                                                                 #
#     :
#                                                                             #
#                                                                             #
#                                                                             #
###############################################################################
#                           Identify Script Home                              #
###############################################################################
#Find the script file home
pushd . > /dev/null
SCRIPT_DIRECTORY="${BASH_SOURCE[0]}";
while([ -h "${SCRIPT_DIRECTORY}" ]);
do
  cd "`dirname "${SCRIPT_DIRECTORY}"`"
  SCRIPT_DIRECTORY="$(readlink "`basename "${SCRIPT_DIRECTORY}"`")";
done
cd "`dirname "${SCRIPT_DIRECTORY}"`" > /dev/null
SCRIPT_DIRECTORY="`pwd`";
popd  > /dev/null
MODULE_HOME="`dirname "${SCRIPT_DIRECTORY}"`"
###############################################################################
#                           Import Dependencies                               #
###############################################################################

#Load common dependencies
. ${MODULE_HOME}/bin/import-dependecies.sh

###############################################################################
#                                CODE                                         #
###############################################################################


while getopts ":t:f:" opt; do
  case $opt in
    t) SYNC_TABLE="$OPTARG"
    ;;
    f) FLOW_NAME="$OPTARG"
    ;;
    *) echo "Invalid option -$OPTARG"
    ;;
  esac
done


MODULE_CLASSPATH=""
JAR_PATH=${SHARED_LIB}/utility.jar

  for i in $module_home/lib/*.jar; do
    MODULE_CLASSPATH=$MODULE_CLASSPATH:$i
  done

  MODULE_CLASSPATH=`echo $MODULE_CLASSPATH | cut -c2-`

  SUCCESS_MESSAGE='Successfully completed schema validation.'
  FAILURE_MESSAGE='Failed to complete schema validation'
  CONFIG_FILE_PATH=${CONFIG_HOME}/${FLOW_NAME}"-"${SYNC_TABLE}"/sync-configurations.xml"

  java -cp ${JAR_PATH} com.utility.migrator.shema_validator.SchemaValidator -t ${SYNC_TABLE} -c ${CONFIG_FILE_PATH}

  exit_code=`fn_get_exit_code $?`
  fn_handle_exit_code_modules "${exit_code}" "${SUCCESS_MESSAGE}" "${FAILURE_MESSAGE}" "${FAIL_ON_ERROR}" "${FLOW_NAME}" "${SYNC_TABLE}"

###############################################################################
#                                     End                                     #
###############################################################################
