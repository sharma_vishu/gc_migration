#!/bin/bash
###############################################################################
#                               Documentation                                 #
###############################################################################
#                                                                             #
# Description                                                                 #
#     :
#                                                                             #
#                                                                             #
#                                                                             #
###############################################################################
#                           Identify Script Home                              #
###############################################################################
#Find the script file home
pushd . > /dev/null
SCRIPT_DIRECTORY="${BASH_SOURCE[0]}";
while([ -h "${SCRIPT_DIRECTORY}" ]);
do
  cd "`dirname "${SCRIPT_DIRECTORY}"`"
  SCRIPT_DIRECTORY="$(readlink "`basename "${SCRIPT_DIRECTORY}"`")";
done
cd "`dirname "${SCRIPT_DIRECTORY}"`" > /dev/null
SCRIPT_DIRECTORY="`pwd`";
popd  > /dev/null
MODULE_HOME="`dirname "${SCRIPT_DIRECTORY}"`"
###############################################################################
#                           Import Dependencies                               #
###############################################################################

#Load common dependencies
. ${MODULE_HOME}/bin/import-dependecies.sh

###############################################################################
#                                CODE                                         #
###############################################################################

while getopts ":t:f:" opt; do
  case $opt in
    t) SYNC_TABLE="$OPTARG"
    ;;
    f) FLOW_NAME="$OPTARG"
    ;;
    *) echo "Invalid option -$OPTARG"
    ;;
  esac
done

CONFIG_FILE_PATH=${CONFIG_HOME}/${FLOW_NAME}"-"${SYNC_TABLE}"/sync-configurations.xml"

#Delete the Hive Based Text & Incremental Table
hive -e "DROP TABLE IF EXISTS ${SYNC_TABLE}_text" &
hive -e "DROP TABLE IF EXISTS ${SYNC_TABLE}_incremental" &
echo "Dropping Hive based Tables" wait
echo "Successfully Completed deleting the Hive based text table and incremental table"

#Cleaning up of the BQ Staging Table
BQ_DATASET=$(java -cp ${SHARED_LIB}/utility.jar com.utility.migrator.loader_utility.FetchBQName -t ${SYNC_TABLE} -c ${CONFIG_FILE_PATH})
BQ_STAGE_TABLE=${BQ_DATASET}"_stage"
bq rm -f $BQ_STAGE_TABLE

#Update the status of the job
java -cp ${SHARED_LIB}/utility.jar com.utility.migrator.sql_utility.JobTaskUpdater -f ${FLOW_NAME} -t ${SYNC_TABLE} -jst "successfully-completed-migrational-transfer" -jss SUCCESSFUL -c ${CONFIG_FILE_PATH}

#Cleaning Up configuration file path
CONF_FILE_PATH=${CONFIG_HOME}/${FLOW_NAME}"-"${SYNC_TABLE}
rm -rf ${CONFIG_HOME}/${FLOW_NAME}"-"${SYNC_TABLE}

EXIT_CODE=`fn_get_exit_code $?`
fn_handle_exit_code_modules "${EXIT_CODE}" "Migrational Task completed successfully." "Failed to persist and/or complete the cleanup tasks." "${FAIL_ON_ERROR}" "${FLOW_NAME}" "${SYNC_TABLE}"


##############################################################################
#                                     End                                     #
###############################################################################
