package com.utility.migrator.shema_validator;

import com.utility.migrator.xml_parser.ConfigurationXMLParser;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.hive.jdbc.HiveDriver;

public class HiveUtilities {
    private String driverName = "org.apache.hive.jdbc.HiveDriver";
    private ConfigurationXMLParser configurationXMLParser = new ConfigurationXMLParser();

    public Statement getHiveConnection(String tableType, String confFilePath) throws Exception{
        Class.forName(driverName);
        try {
            String hiveUrl="",hiveUser="",hivePassword="";

            if (tableType.equals("source")){
                hiveUrl = configurationXMLParser.getXMLProperty("clusters.src.hive.jdbc", confFilePath);
                hiveUser = configurationXMLParser.getXMLProperty("clusters.src.hive.user", confFilePath);
                hivePassword = configurationXMLParser.getXMLProperty("clusters.src.hive.password", confFilePath);
            }
            else if (tableType.equals("destination")) {
                hiveUrl = configurationXMLParser.getXMLProperty("clusters.dest.hive.jdbc", confFilePath);
                hiveUser = configurationXMLParser.getXMLProperty("clusters.dest.hive.user", confFilePath);
                hivePassword = configurationXMLParser.getXMLProperty("clusters.dest.hive.password", confFilePath);
            }
            else {
                System.out.println("None of the configuration values caught for Hive JDBC, Hive User and Hive Password");
                System.exit(1);
            }

            Connection conn;
            conn = DriverManager.getConnection(hiveUrl, hiveUser, hivePassword);
            return conn.createStatement();
        } catch (SQLException sqle){
            sqle.printStackTrace();
            return null;
        }
    }

    public void HiveRunner(String tableType, String query, String confFilePath) throws Exception {
        Class.forName(driverName);

        String hiveUrl="",hiveUser="",hivePassword="";

        if (tableType.equals("source")){
            hiveUrl = configurationXMLParser.getXMLProperty("clusters.src.hive.jdbc", confFilePath);
            hiveUser = configurationXMLParser.getXMLProperty("clusters.src.hive.user", confFilePath);
            hivePassword = configurationXMLParser.getXMLProperty("clusters.src.hive.password", confFilePath);
        }
        else if (tableType.equals("destination")) {
            hiveUrl = configurationXMLParser.getXMLProperty("clusters.dest.hive.jdbc", confFilePath);
            hiveUser = configurationXMLParser.getXMLProperty("clusters.dest.hive.user", confFilePath);
            hivePassword = configurationXMLParser.getXMLProperty("clusters.dest.hive.password", confFilePath);
        }
        else {
            System.out.println("None of the configuration values caught for Hive JDBC, Hive User and Hive Password");
            System.exit(1);
        }

        Connection con = DriverManager.getConnection(hiveUrl, hiveUser, hivePassword);
        Statement stmt = con.createStatement();

        System.out.println("Executing Hive Query = " + query);

        stmt.execute(query);
    }

    public void HiveIteratorList(String tableType, Iterator item, String confFilePath) throws  Exception {

        while(item.hasNext()){
            String qry = item.next().toString();
            HiveRunner(tableType, qry, confFilePath);
        }
    }

    public String getTableSchema(String tableType, String hiveTblName, String confFilePath) throws Exception {

        Statement st = getHiveConnection(tableType, confFilePath);
        System.out.println("show create table " + hiveTblName);
        ResultSet rs = st.executeQuery("show create table "+ hiveTblName);

        String tableDef="";
        while (rs.next()) {
            if (rs.getString(1).equals("LOCATION"))
                break;
            else
                tableDef +=  rs.getString(1) + " ";
        }

        return tableDef;
    }

    public Map<String,ArrayList<String>> hiveSchemaDetails(String tableType, String query, String confFilePath) throws Exception {

        String hiveUrl="",hiveUser="",hivePassword="";

        if (tableType.equals("source")){
            hiveUrl = configurationXMLParser.getXMLProperty("clusters.src.hive.jdbc", confFilePath);
            hiveUser = configurationXMLParser.getXMLProperty("clusters.src.hive.user", confFilePath);
            hivePassword = configurationXMLParser.getXMLProperty("clusters.src.hive.password", confFilePath);
        }
        else if (tableType.equals("destination")) {
            hiveUrl = configurationXMLParser.getXMLProperty("clusters.dest.hive.jdbc", confFilePath);
            hiveUser = configurationXMLParser.getXMLProperty("clusters.dest.hive.user", confFilePath);
            hivePassword = configurationXMLParser.getXMLProperty("clusters.dest.hive.password", confFilePath);
        }
        else {
            System.out.println("None of the configuration values caught for Hive JDBC, Hive User and Hive Password");
            System.exit(1);
        }

        Map<String,ArrayList<String>> ret_values = new HashMap();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(hiveUrl, hiveUser, hivePassword);
            Statement stmt = con.createStatement();

            ArrayList<String> columns = new ArrayList<>();
            ArrayList<String> datatypes = new ArrayList<>();

            System.out.println("Executing Query = " + query);
            ResultSet rs = stmt.executeQuery(query);

            ResultSetMetaData metaData = rs.getMetaData();

            //To get the total column count
            int columnCount = metaData.getColumnCount();
            System.out.println(columnCount);

            while (rs.next()) {
                if(rs.getString(metaData.getColumnName(2)) == null) {
                    break;
                }
                columns.add(rs.getString(metaData.getColumnName(1)));
                datatypes.add(rs.getString(metaData.getColumnName(2)));
            }
            ret_values.put("columns", columns);
            ret_values.put("data_types", datatypes);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (Exception ex) {
            System.out.println("Some form of error has occurred .....");
            ex.printStackTrace();
        }
        return ret_values;
    }

    public Boolean validateHiveTableExists (String tableType, String hiveTableName, String confFilePath) throws Exception {

        String hiveUrl, hiveUser, hivePassword;

        if (tableType.equals("destination")) {
            hiveUrl = configurationXMLParser.getXMLProperty("clusters.dest.hive.jdbc", confFilePath);
            hiveUser = configurationXMLParser.getXMLProperty("clusters.dest.hive.user", confFilePath);
            hivePassword = configurationXMLParser.getXMLProperty("clusters.dest.hive.password", confFilePath);
        } else {
            hiveUrl = configurationXMLParser.getXMLProperty("clusters.dest.hive.jdbc", confFilePath);
            hiveUser = configurationXMLParser.getXMLProperty("clusters.dest.hive.user", confFilePath);
            hivePassword = configurationXMLParser.getXMLProperty("clusters.dest.hive.password", confFilePath);
        }
        Class.forName(driverName);
        Connection con = DriverManager.getConnection(hiveUrl, hiveUser, hivePassword);
        DatabaseMetaData databaseMetaData = con.getMetaData();
        ResultSet resultSet = databaseMetaData.getTables(null, hiveTableName.substring(0, hiveTableName.indexOf(".")),
                hiveTableName.substring(hiveTableName.indexOf(".")+1, hiveTableName.length()), null);

        if (!resultSet.next())
            return false;
        else
            return true;
    }
}
