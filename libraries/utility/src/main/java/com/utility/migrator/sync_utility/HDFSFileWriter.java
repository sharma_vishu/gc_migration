package com.utility.migrator.sync_utility;

import com.utility.migrator.xml_parser.ConfigurationXMLParser;
import org.apache.commons.cli.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.util.Progressable;

import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import static java.lang.System.out;

public class HDFSFileWriter {
    private static ConfigurationXMLParser configurationXMLParser = new ConfigurationXMLParser();
    private static String driverName = "org.apache.hive.jdbc.HiveDriver";

    public static void main(String[] args) throws Exception {

        Options options = new Options();

        Option table_name = new Option("t", "sync_table_name", true, "sync table name");
        Option config_file_path = new Option("c", "config_file_path", true, "config file path");

        table_name.setRequired(true);
        config_file_path.setRequired(true);

        options.addOption(table_name);
        options.addOption(config_file_path);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
            return;
        }

        String sourceTableName = cmd.getOptionValue("sync_table_name");
        String confFile = cmd.getOptionValue("config_file_path");

        writeToHdfs(confFile, sourceTableName);
    }

    public static void writeToHdfs (String confFilePath, String sourceTableName) throws Exception {

        String tableLocation = getTableSchema(sourceTableName, confFilePath).replace("'", "").trim();
        System.out.println(tableLocation);

        String hdfsURI =  tableLocation.substring(0, tableLocation.indexOf("/", tableLocation.lastIndexOf(":")));

        Configuration configuration = new Configuration();
        FileSystem hdfs = FileSystem.get( new URI( hdfsURI ), configuration );
        Path file = new Path(hdfsURI.concat("/migrator/").concat(sourceTableName));
        if ( hdfs.exists( file ))
        { hdfs.delete( file, true );
        }
        OutputStream os = hdfs.create( file,
                new Progressable() {
                    public void progress() {
                        out.println("...bytes written: []");
                    } });
        BufferedWriter br = new BufferedWriter( new OutputStreamWriter( os, "UTF-8" ) );
        br.write(sourceTableName);
        br.close();
        hdfs.close();
    }

    public static String getTableSchema(String hiveTblName, String confFilePath) throws Exception {

        Class.forName(driverName);

        String hiveJdbcURL = configurationXMLParser.getXMLProperty("clusters.dest.hive.jdbc", confFilePath);
        String hiveUser = configurationXMLParser.getXMLProperty("clusters.dest.hive.user", confFilePath);
        String hivePassword = configurationXMLParser.getXMLProperty("clusters.dest.hive.password", confFilePath);

        Connection con = DriverManager.getConnection(hiveJdbcURL, hiveUser, hivePassword);
        Statement stmt = con.createStatement();

        ResultSet rs = stmt.executeQuery("show create table "+ hiveTblName);

        String tblLocation="";
        while (rs.next()) {
            if (rs.getString(1).equals("LOCATION")){
                rs.next();
                tblLocation = rs.getString(1);
                break;
            }
        }
         return tblLocation;
    }
}
