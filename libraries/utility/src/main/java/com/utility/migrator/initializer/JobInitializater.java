package com.utility.migrator.initializer;

import com.utility.migrator.xml_parser.ConfigurationXMLParser;
import org.apache.commons.cli.*;
import org.joda.time.DateTime;

import org.joda.time.format.DateTimeFormat;

import java.sql.PreparedStatement;
import java.util.Calendar;

public class JobInitializater {
    public static void main(String[] args) throws Exception {
        Options options = new Options();

        Option flow_name = new Option("f", "flow_name", true, "flow name");
        Option table_name = new Option("t", "sync_table_name", true, "sync table name");
        Option config_file_path = new Option("c", "config_file_path", true, "config file path");

        flow_name.setRequired(true);
        table_name.setRequired(true);
        config_file_path.setRequired(true);

        options.addOption(flow_name);
        options.addOption(table_name);
        options.addOption(config_file_path);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
            return;
        }

        String flowName = cmd.getOptionValue("flow_name");
        String tableName = cmd.getOptionValue("sync_table_name");
        String confFilePath = cmd.getOptionValue("config_file_path");

        SQLUtility sqlUtility = new SQLUtility();
        ConfigurationXMLParser configurationXMLParser = new ConfigurationXMLParser();

        System.out.println("Accepted config Path As - " + confFilePath);

        String sqlDbName = configurationXMLParser.getXMLProperty("clusters.mysql.db.name", confFilePath);
        String sqlTableName = configurationXMLParser.getXMLProperty("clusters.mysql.flow.table.name", confFilePath);
        String mySqlTableName = sqlDbName+"."+sqlTableName;
//TODO need to handle jobs status in mysql table
        String checkFlowExistsString = "select count(*) from " + mySqlTableName + " where flow_name='" + flowName +
                "' and table_name='" + tableName + "' and status='RUNNING'";
        System.out.println(checkFlowExistsString);
//TODO write logger.info and use string builder
        int count = sqlUtility.getCount(checkFlowExistsString, confFilePath);
        if (count != 0){
            //Job should terminate immediately at this stage
            System.out.println("The another instance of this flow for the table " + tableName + " is already in running stage. Exiting now !");
            System.exit(121);
        }
        else {
            //Persist this particular run into the sql table
            String jobStage = "Stage 1 : Job Initialization";
            String loader_timestamp = DateTimeFormat.forPattern("yyyy-MM-dd-HH:mm:ss").print(new DateTime(Calendar.getInstance()));

            // the mysql insert statement
            //TODO check status from azkaban first then insert into mysql table
            String query = "INSERT INTO " + mySqlTableName + " (flow_name, table_name, job_stage, status, loader_timestamp)"
                    + " values (?, ?, ?, ?,?)";

            PreparedStatement preparedStmt = sqlUtility.getSQLConnection(confFilePath).prepareStatement(query);

            int itr=0;
            preparedStmt.setString (++itr, flowName);
            preparedStmt.setString (++itr, tableName);
            preparedStmt.setString (++itr, jobStage);
            preparedStmt.setString (++itr, "RUNNING");
            preparedStmt.setString (++itr, loader_timestamp);

            boolean status = preparedStmt.execute();
            if (!status)
                System.out.println("Flow has been successfully registered for table - " + tableName);
            else
                System.out.println("Failed to register the flow");
        }
    }
}
