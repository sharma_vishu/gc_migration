package com.utility.migrator.initializer;

import com.utility.migrator.xml_parser.ConfigurationXMLParser;
import java.sql.*;

public class SQLUtility {
    ConfigurationXMLParser configurationXMLParser = new ConfigurationXMLParser();

    public Connection getSQLConnection(String confFilePath) throws Exception {
        try {
            /*//TODO : For Cloud SQL
            System.out.println("SQL Table Name = " + sqlTableName);
            int indexedTable = sqlTableName.indexOf('.');

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String jdbcUrl = String.format(
                    "jdbc:mysql://google/%s?cloudSqlInstance=%s&"
                            + "socketFactory=com.google.cloud.sql.mysql.SocketFactory",
                    sqlTableName.substring(0, indexedTable),
                    instanceConnectionName);

            Connection connection = DriverManager.getConnection(jdbcUrl, user, password);
            return connection.createStatement();*/

            //TODO : For MYSQL Connection
            String jdbcUrl = configurationXMLParser.getXMLProperty("clusters.mysql.host", confFilePath);
            String sqlUser = configurationXMLParser.getXMLProperty("clusters.mysql.user", confFilePath);
            String sqlPassword = configurationXMLParser.getXMLProperty("clusters.mysql.password", confFilePath);

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection connection = DriverManager.getConnection(jdbcUrl, sqlUser, sqlPassword);
            return connection;

        } catch (SQLException sqle){
            sqle.printStackTrace();
            return null;
        }
    }


    public int getCount (String queryString, String confFilePath) throws Exception{

        Statement stmt = new SQLUtility().getSQLConnection(confFilePath).createStatement();
        ResultSet checkFlowExists = stmt.executeQuery(queryString);
        int count = -1;
        while (checkFlowExists.next()){
            count = checkFlowExists.getInt(1);
        }
        return count;
    }
}
