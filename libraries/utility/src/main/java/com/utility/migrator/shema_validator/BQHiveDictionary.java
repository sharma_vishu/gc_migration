package com.utility.migrator.shema_validator;

import java.util.HashMap;

public class BQHiveDictionary {
    public static HashMap<String, String> columnDictionary = new HashMap<>();

    static {
        columnDictionary.put("string", "STRING");
        columnDictionary.put("varchar", "STRING");
        columnDictionary.put("char", "STRING");
        columnDictionary.put("int", "INTEGER");
        columnDictionary.put("bigint", "INTEGER");
        columnDictionary.put("tinyint", "INTEGER");
        columnDictionary.put("smallint", "INTEGER");
        columnDictionary.put("double", "FLOAT");
        columnDictionary.put("decimal", "FLOAT");
        columnDictionary.put("boolean", "BOOLEAN");
        columnDictionary.put("STRUCT", "RECORD");
        columnDictionary.put("date", "STRING");
        columnDictionary.put("timestamp", "STRING");
        columnDictionary.put("BINARY", "BYTES");
        columnDictionary.put("array", "RECORD");

    }
}