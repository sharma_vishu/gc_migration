package com.utility.migrator.sync_utility;

import com.utility.migrator.initializer.SQLUtility;
import com.utility.migrator.xml_parser.ConfigurationXMLParser;
import org.apache.commons.cli.*;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.io.*;
import java.sql.*;
import java.util.*;

public class TextTableLoader {
    private static ConfigurationXMLParser configurationXMLParser = new ConfigurationXMLParser();
    private static String driverName = "org.apache.hive.jdbc.HiveDriver";
    static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(TextTableLoader.class);

    public static void main(String[] args) throws Exception {

        Options options = new Options();

        Option flow_name = new Option("f", "flow_name", true, "flow name");
        Option table_name = new Option("t", "sync_table_name", true, "sync table name");
        Option template_file_path = new Option("tfp", "template_file_path", true, "template file path");
        Option config_file_path = new Option("c", "config_file_path", true, "config file path");
        Option avro_file_path = new Option("afp", "avro_file_path", true, "config file path");

        flow_name.setRequired(true);
        table_name.setRequired(true);
        template_file_path.setRequired(true);
        config_file_path.setRequired(true);
        avro_file_path.setRequired(true);

        options.addOption(flow_name);
        options.addOption(table_name);
        options.addOption(template_file_path);
        options.addOption(config_file_path);
        options.addOption(avro_file_path);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
            return;
        }

        String flowName = cmd.getOptionValue("flow_name");
        String destinationTable = cmd.getOptionValue("sync_table_name");
        String templateFilePath = cmd.getOptionValue("template_file_path");
        String confFilePath = cmd.getOptionValue("config_file_path");
        String avroFilePath = cmd.getOptionValue("avro_file_path");

        //Count of this table helps to understand if its a batch or incremental table
        String mySqlTableName = configurationXMLParser.getXMLProperty("clusters.mysql.db.name", confFilePath)
                .concat(".").concat(configurationXMLParser.getXMLProperty("clusters.mysql.flow.table.name",confFilePath));

        VelocityEngine ve = new VelocityEngine();
        Properties props = new Properties();

        int fileStartIndex = templateFilePath.lastIndexOf("/") + 1;
        String filePath = templateFilePath.substring(0, fileStartIndex - 1);
        String fileName = templateFilePath.substring(fileStartIndex, templateFilePath.length());

        props.put("file.resource.loader.path", filePath);
        props.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.NullLogSystem");
        ve.init(props);

        Template template = ve.getTemplate(fileName);
        VelocityContext context = new VelocityContext();

        String textTableName = destinationTable.concat("_text");
        String tableSchema = TableSchema("describe ".concat(destinationTable), confFilePath);
        String tableLocation = configurationXMLParser.getXMLProperty("airbnb.reair.clusters.dest.hdfs.root", confFilePath).concat("_txt");

        //Get the Schema Details of the AVRO Table to be plugged into the Table Properties

        //context.put("tvar_COLUM_DETAILS", tableSchema);
        context.put("tvar_TABLE_NAME", textTableName);
        context.put("tvar_TABLE_LOCATION", tableLocation);
        context.put("tvar_HDFS_FILE_PATH", avroFilePath);

        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        String textFileSchema = writer.toString();

        String checkFlowExistsString = "select count(*) from " + mySqlTableName + " where flow_name='" + flowName +
                "' and table_name='" + destinationTable + "' and status='SUCCESSFUL'";
        System.out.println(checkFlowExistsString);
        int count = new SQLUtility().getCount(checkFlowExistsString, confFilePath);

        // TODO : TAKE CARE OF THE BELOW SHIT
        // Preparing the loader table both for Partitioned or Non-Partitioned Table
        String sqlPartitionedTableInfo = new ConfigurationXMLParser().getXMLProperty("clusters.mysql.db.name", confFilePath)
                .concat(".").concat(new ConfigurationXMLParser()
                        .getXMLProperty("clusters.mysql.partitioned.table.info.name", confFilePath));


        ResultSet resultSet = new SQLUtility().getSQLConnection(confFilePath).createStatement()
                .executeQuery("select partitioned, partition_column, partition_format from " + sqlPartitionedTableInfo + " where table_name='" +
                        destinationTable + "'");

        String isPartitioned="", partitionedColumnName="", partitionedFormat="";
        while (resultSet.next()){
            isPartitioned = resultSet.getString(1);
            partitionedColumnName = resultSet.getString(2);
            partitionedFormat = resultSet.getString(3);
        }

        String loaderTemplate;
        String tablePartition = "";

        if (isPartitioned.toLowerCase().equals("yes")) {
            loaderTemplate = "INSERT OVERWRITE TABLE TEXT_TABLE_NAME PARTITION(ingestion_time) SELECT " + tableSchema +
                    " cast(from_unixtime(unix_timestamp(cast(" + partitionedColumnName + " AS string),'"+ partitionedFormat + "'),'yyyyMMdd') AS bigint) AS ingestion_time " +
                    "FROM DEST_TABLE_NAME";
        }
        else {
            loaderTemplate = "INSERT OVERWRITE TABLE TEXT_TABLE_NAME PARTITION(PARTITION_VALUES) SELECT * FROM DEST_TABLE_NAME";
            tablePartition = "ingestion_time=" + DateTimeFormat.forPattern("yyyyMMdd").print(new DateTime(Calendar.getInstance()));
        }

        loaderTemplate = loaderTemplate
                .replace("TEXT_TABLE_NAME", textTableName)
                .replace("PARTITION_VALUES", tablePartition)
        ;


        //To understand if to pick up full or incremental data
        String hqlTemplateFile;
        if (count == 0) {
            //BatchRun
            loaderTemplate = loaderTemplate.replace("DEST_TABLE_NAME", destinationTable);
            hqlTemplateFile = "Overwrite-Batch.vm";
        } else {
            //Incremental Run
            loaderTemplate = loaderTemplate.replace("DEST_TABLE_NAME", destinationTable.concat("_incremental"));
            hqlTemplateFile = "Overwrite-Incremental.vm";
        }

        //Populate the contents in the Incremental file
        VelocityContext hiveQueryContext = new VelocityContext();
        Template hiveQueryTemplate = ve.getTemplate(hqlTemplateFile);
        hiveQueryContext.put("tvar_HIVE_QUERY",loaderTemplate);
        StringWriter queryWriter = new StringWriter();
        hiveQueryTemplate.merge(hiveQueryContext, queryWriter);
        String query = queryWriter.toString();

        String hqlPath = confFilePath.substring(0,confFilePath.lastIndexOf("/"));
        String hqlFile = hqlPath+"/hiveQuery.hql";
        FileWriter fileWriter = new FileWriter(hqlFile);
        fileWriter.write(query);
        fileWriter.close();

        HiveRunner("DROP TABLE IF EXISTS " + textTableName, confFilePath);
        HiveRunner(textFileSchema, confFilePath);
        LOG.info("Insert Overwrite Query is running " + query);
        Runtime rt = Runtime.getRuntime();
        Process proc = rt.exec("hive -f " + hqlFile );
        proc.waitFor();
        LOG.info("exit code "+proc.exitValue());

    }

    public static void HiveRunner(String query, String confFilePath) throws Exception {
        Class.forName(driverName);
        String hiveJdbcURL = configurationXMLParser.getXMLProperty("clusters.dest.hive.jdbc", confFilePath);
        String hiveUser = configurationXMLParser.getXMLProperty("clusters.dest.hive.user", confFilePath);
        String hivePassword = configurationXMLParser.getXMLProperty("clusters.dest.hive.password", confFilePath);

        Connection con = DriverManager.getConnection(hiveJdbcURL, hiveUser, hivePassword);
        Statement stmt = con.createStatement();

        System.out.println(query);
        stmt.execute("SET hive.exec.dynamic.partition.mode=nonstrict");
        stmt.execute(query);
    }

    public static String TableSchema(String query, String confFilePath) {

        String finalTableSchema="",adjustedTableSchema="";
        try {
            Class.forName(driverName);
            String hiveJdbcURL = configurationXMLParser.getXMLProperty("clusters.dest.hive.jdbc", confFilePath);
            String hiveUser = configurationXMLParser.getXMLProperty("clusters.dest.hive.user", confFilePath);
            String hivePassword = configurationXMLParser.getXMLProperty("clusters.dest.hive.password", confFilePath);

            Connection con = DriverManager.getConnection(hiveJdbcURL, hiveUser, hivePassword);
            Statement stmt = con.createStatement();

            System.out.println("Executing Query = " + query);
            ResultSet rs = stmt.executeQuery(query);

            ResultSetMetaData metaData = rs.getMetaData();
            //To get the total column count
            int columnCount = metaData.getColumnCount();
            System.out.println(columnCount);

            while (rs.next()) {
                if(rs.getString(metaData.getColumnName(2)) == null) {
                    break;
                }
                //finalTableSchema += rs.getString(metaData.getColumnName(1))+" " + rs.getString(metaData.getColumnName(2))+",";
                finalTableSchema += rs.getString(metaData.getColumnName(1))+ ", ";
            }

            adjustedTableSchema = finalTableSchema.substring(0,finalTableSchema.length()-1);

            System.out.println(adjustedTableSchema);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (Exception ex) {
            System.out.println("Some form of error has occurred .....");
            ex.printStackTrace();
        }
        return adjustedTableSchema;
    }
}