package com.utility.migrator.loader_utility;

import com.utility.migrator.initializer.SQLUtility;
import org.apache.commons.cli.*;

import java.sql.ResultSet;

public class FetchBQName {
    public static void main(String[] args) throws Exception {

        Options options = new Options();

        Option ddhSchema = new Option("t", "ddh_schema_table", true, "ddh schema");
        Option confFilePath = new Option("c", "config_file_path", true, "config file path");

        ddhSchema.setRequired(true);
        confFilePath.setRequired(true);

        options.addOption(ddhSchema);
        options.addOption(confFilePath);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
            return;
        }

        String ddh_schema = cmd.getOptionValue("ddh_schema_table");
        String conf_File_Path = cmd.getOptionValue("config_file_path");


        ResultSet resultSet = new SQLUtility().getSQLConnection(conf_File_Path).createStatement()
                .executeQuery("select bq_schema_name from db_migration.bq_dictionary where ddh_table_name='" + ddh_schema + "' LIMIT 1");
        String dataSetName="";
        while (resultSet.next())
            dataSetName = resultSet.getString(1);

        String tableName = ddh_schema.split("\\.")[1];
        String finalBQName = dataSetName.concat("." + tableName);
        System.out.println(finalBQName);
    }
}
