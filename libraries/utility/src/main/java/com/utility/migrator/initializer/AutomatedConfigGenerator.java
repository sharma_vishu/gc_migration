package com.utility.migrator.initializer;

import org.apache.commons.cli.*;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.StringWriter;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class AutomatedConfigGenerator {
    private static String driverName = "org.apache.hive.jdbc.HiveDriver";

    public static void main(String[] args) throws Exception {

        /**
         * Args[0] - Configuration Property File
         * Args[1] - Velocity Template File Location
         * Args[2] - Table being synced
         * Args[3] - Location where configuration file will be kept
         * Args[4] - Flow Name
         * "/Users/tkmah9c/GoogleCloudMigration/GCDataMovement/bigdata-sync/config/configurations.properties"
         * "/Users/tkmah9c/GoogleCloudMigration/GCDataMovement/bigdata-sync/modules/of-flow-initializer/src/main/resources/reair-configurations.vm"
         * "adityadb.table1" "/Users/tkmah9c/GoogleCloudMigration/GCDataMovement/bigdata-sync/config/dev" "of-table-migrator"
         */

        Options options = new Options();
        Option configuration_Property_File = new Option("cf", "configuration_prop_file", true, "configuration property file");
        Option velocity_Template = new Option("vm", "velocity_temp_file", true, "velocity template file");
        Option table_name = new Option("t", "sync_table_name", true, "sync table name");
        Option config_file_path = new Option("c", "config_file_path", true, "config file path");
        Option flow_name = new Option("f", "flow_name", true, "flow name");


        configuration_Property_File.setRequired(true);
        velocity_Template.setRequired(true);
        table_name.setRequired(true);
        config_file_path.setRequired(true);
        flow_name.setRequired(true);


        options.addOption(configuration_Property_File);
        options.addOption(velocity_Template);
        options.addOption(table_name);
        options.addOption(config_file_path);
        options.addOption(flow_name);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
            return;
        }

        String configurationPropertyFile = cmd.getOptionValue("configuration_prop_file");
        String velocityTemplate = cmd.getOptionValue("velocity_temp_file");
        String tableName = cmd.getOptionValue("sync_table_name");
        String confFilePath = cmd.getOptionValue("config_file_path");
        String flowName = cmd.getOptionValue("flow_name");


        String dbName = tableName.substring(0,tableName.indexOf("."));

        Properties configurations = new Properties();
        configurations.load(new FileInputStream(new File(configurationPropertyFile)));

        Map<String, String> connectionsJDBC = new HashMap<>();

        String thriftSourceURL = configurations.getProperty("TVAR_THRIFT_SRC_URL");
        String thriftDestinationURL = configurations.getProperty("TVAR_THRIFT_DEST_URL");
        String sourceHiveJDBC = configurations.getProperty("TVAR_SRC_HIVE_JDBC");
        String sourceHiveUser = configurations.getProperty("TVAR_SRC_HIVE_USER");
        String sourceHivePassword = configurations.getProperty("TVAR_SRC_HIVE_PASSWORD");
        String destinationHiveJDBC = configurations.getProperty("TVAR_DEST_HIVE_JDBC");
        String destinationHiveUser = configurations.getProperty("TVAR_DEST_HIVE_USER");
        String destinationHivePassword = configurations.getProperty("TVAR_DEST_HIVE_PASSWORD");
        String sqlJDBC = configurations.getProperty("TVAR_SQL_JDBC");
        String sqlUser = configurations.getProperty("TVAR_SQL_USER");
        String sqlPassword = configurations.getProperty("TVAR_SQL_PASSWORD");
        String sqlDbName = configurations.getProperty("TVAR_SQL_DB_NAME");
        String sqlFlowTableName = configurations.getProperty("TVAR_SQL_FLOW_TABLE_NAME");
        String sqlSchemaTableName = configurations.getProperty("TVAR_SQL_FLOW_SCHEMA_NAME");
        String bigQueryProjectId = configurations.getProperty("TVAR_BQ_PROJECT_ID");
        //String bigQuerySchemaName = configurations.getProperty("TVAR_BQ_SCHEMA_NAME");
        String azkabanFlowName = configurations.getProperty("TVAR_AZKABAN_FLOW_NAME");
        //GS Base Path fetched from Already defined dictionary
        String googleStorageBasePath = new DictionaryForAutomatedConfigGenerator().getGsBasePath(tableName, configurationPropertyFile);
        String failedPartitionsTableName = configurations.getProperty("TVAR_SQL_FAILED_LOAD_NAME");
        String sqlDictionaryName = configurations.getProperty("TVAR_SQL_FLOW_DICTIONARY_NAME");
        String sqlPartitionedTableInfo = configurations.getProperty("TVAR_SQL_PARTITION_INFO_TBL_NAME");
        String partitionsMetadata = configurations.getProperty("TVAR_SQL_PARTITION_METADATA");

        connectionsJDBC.put("TVAR_SRC_HIVE_JDBC", sourceHiveJDBC);
        connectionsJDBC.put("TVAR_SRC_HIVE_USER", sourceHiveUser);
        connectionsJDBC.put("TVAR_SRC_HIVE_PASSWORD", sourceHivePassword);
        connectionsJDBC.put("TVAR_DEST_HIVE_JDBC", destinationHiveJDBC);
        connectionsJDBC.put("TVAR_DEST_HIVE_USER", destinationHiveUser);
        connectionsJDBC.put("TVAR_DEST_HIVE_PASSWORD", destinationHivePassword);

        String templateFileLocation = velocityTemplate;
        String filePath = templateFileLocation.substring(0, templateFileLocation.lastIndexOf("/"));
        String fileName = templateFileLocation.substring(templateFileLocation.lastIndexOf("/") + 1, templateFileLocation.length());

        System.out.println(filePath);
        System.out.println(fileName);

        VelocityEngine ve = new VelocityEngine();
        Properties velocityProperties = new Properties();
        velocityProperties.put("file.resource.loader.path", filePath);
        velocityProperties.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.NullLogSystem");
        ve.init(velocityProperties);

        Template template = ve.getTemplate(fileName);
        VelocityContext context = new VelocityContext();

        context.put("TVAR_THRIFT_SRC_URL", thriftSourceURL);
        context.put("TVAR_THRIFT_DEST_URL", thriftDestinationURL);
        context.put("TVAR_SRC_HIVE_JDBC", sourceHiveJDBC);
        context.put("TVAR_SRC_HIVE_USER", sourceHiveUser);
        context.put("TVAR_SRC_HIVE_PASSWORD", sourceHivePassword);
        context.put("TVAR_DEST_HIVE_JDBC", destinationHiveJDBC);
        context.put("TVAR_DEST_HIVE_USER", destinationHiveUser);
        context.put("TVAR_DEST_HIVE_PASSWORD", destinationHivePassword);
        context.put("TVAR_SQL_JDBC", sqlJDBC);
        context.put("TVAR_SQL_USER", sqlUser);
        context.put("TVAR_SQL_PASSWORD", sqlPassword);
        context.put("TVAR_SQL_DB_NAME", sqlDbName);
        context.put("TVAR_SQL_FLOW_TABLE_NAME", sqlFlowTableName);
        context.put("TVAR_SQL_FLOW_SCHEMA_NAME", sqlSchemaTableName);
        context.put("TVAR_BQ_PROJECT_ID", bigQueryProjectId);
        context.put("TVAR_BQ_SCHEMA_NAME", new DictionaryForAutomatedConfigGenerator().getBQSchemaName(tableName, configurationPropertyFile));
        context.put("TVAR_AZKABAN_FLOW_NAME", azkabanFlowName);
        context.put("TVAR_SQL_FAILED_LOAD_NAME", failedPartitionsTableName);
        context.put("TVAR_SQL_FLOW_DICTIONARY_NAME", sqlDictionaryName);
        context.put("TVAR_SQL_PARTITION_INFO_TBL_NAME", sqlPartitionedTableInfo);
        context.put("TVAR_SQL_PARTITION_METADATA", partitionsMetadata);

        System.out.println("Finding souce Table Location ....");
        String sourceFsLocation = getSourceTableLocation("source", tableName, connectionsJDBC);

        System.out.println("Finding destination Table Location ....");
        String destinationFsLocation = getDestinationFileSystemLocation(googleStorageBasePath, sourceFsLocation);

        context.put("TVAR_SRC_FS_LOCATION", sourceFsLocation);
        context.put("TVAR_DEST_FS_LOCATION", destinationFsLocation);

        //Setting threads and parallelism based on the nature of the job - batch or incremental
//The parallelism to use for jobs requiring metastore calls. This translates to the number of mappers
        //  or reducers in the relevant jobs.
        //TODO number of mapper and reducer should be defined in property file and set values according to the size of data

        if (!validateHiveTableExists(tableName, destinationHiveJDBC, destinationHiveUser, destinationHivePassword)) {
            context.put("METASTORE_PARALLELISM", 200);
            context.put("BATCH_COPY_PARALLELISM", 400);
        } else {
            context.put("METASTORE_PARALLELISM", 20);
            context.put("BATCH_COPY_PARALLELISM", 40);
        }

        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        String configurationXML = writer.toString();

        //System.out.println(configurationXML);
        String fileQualifiedPath = confFilePath.concat("/" + flowName + "-" + tableName);
        String configurationFileName = fileQualifiedPath.concat("/" + "sync-configurations.xml");
//TODO implement check file path first
        Runtime rt = Runtime.getRuntime();
        Process procDelete = rt.exec("rm -rf " + fileQualifiedPath);
        procDelete.waitFor();
        Process procCreate = rt.exec("mkdir -p " + fileQualifiedPath);
        procCreate.waitFor();

        FileWriter fileWriter = new FileWriter(new File(configurationFileName));
        fileWriter.write(configurationXML);

        fileWriter.close();

    }

    public static String getDestinationFileSystemLocation(String basePath, String sourceFsLocation) {
        //Get the source basePath and tableName and create a structure like basePath/dbName/tableName

        String sourceFsLocation_tmp = sourceFsLocation.substring(sourceFsLocation.indexOf("//")+2, sourceFsLocation.length());
        String sourceBasePath = sourceFsLocation_tmp.substring(sourceFsLocation_tmp.indexOf("/"), sourceFsLocation_tmp.length());
        return basePath.concat(sourceBasePath);
    }

    public static String getSourceTableLocation(String tableType, String hiveTableName, Map<String, String> connectionsJDBC) throws Exception {

        Class.forName(driverName);

        String hiveUrl, hiveUser, hivePassword;
        hiveUrl = connectionsJDBC.get("TVAR_SRC_HIVE_JDBC");
        hiveUser = connectionsJDBC.get("TVAR_SRC_HIVE_USER");
        hivePassword = connectionsJDBC.get("TVAR_SRC_HIVE_PASSWORD");

        System.out.println(hiveUrl + "\t" + hiveUser + "\t" + hivePassword);

        Connection con = DriverManager.getConnection(hiveUrl, hiveUser, hivePassword);
        Statement stmt = con.createStatement();

        ResultSet rs = stmt.executeQuery("show create table " + hiveTableName);
        System.out.println("show create table " + hiveTableName);

        String tableLocation = "";
        while (rs.next()) {
            if (rs.getString(1).equals("LOCATION")) {
                rs.next();
                tableLocation = rs.getString(1).replace("'", "").trim();
                System.out.println(tableLocation);
                break;
            }
        }
        return tableLocation;
    }

    public static Boolean validateHiveTableExists(String hiveTableName, String hiveUrl, String hiveUser, String hivePassword) throws Exception {

        System.out.println(hiveUrl+"\t"+hiveUser+"\t"+hivePassword);
        Class.forName(driverName);
        Connection con = DriverManager.getConnection(hiveUrl, hiveUser, hivePassword);
        DatabaseMetaData databaseMetaData = con.getMetaData();
        ResultSet resultSet = databaseMetaData.getTables(null, hiveTableName.substring(0, hiveTableName.indexOf(".")),
                hiveTableName.substring(hiveTableName.indexOf(".") + 1, hiveTableName.length()), null);

        if (!resultSet.next())
            return false;
        else
            return true;
    }
}