package com.utility.migrator.initializer;

import org.apache.commons.cli.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import com.mysql.jdbc.Driver;

/**
 * Created by tkmah9c on 6/12/17.
 */
public class RunSQLFile {
    public static void main(String[] args) throws Exception {
        Class.forName("com.mysql.jdbc.Driver").newInstance();

        Options options = new Options();
        Option sql_file_path = new Option("f", "sql_file_path", true, "sql file path");
        Option property_file_path = new Option("p", "property_file_path", true, "property file path");
        sql_file_path.setRequired(true);
        property_file_path.setRequired(true);
        options.addOption(sql_file_path);
        options.addOption(property_file_path);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
            return;
        }

        String sqlFile = cmd.getOptionValue("sql_file_path");
        String propertiesFile = cmd.getOptionValue("property_file_path");

        //Get the mysql file to be executed under sql schema
        BufferedReader br = new BufferedReader(new FileReader(new File(sqlFile)));
        String sql_schema="",line;
        while ((line = br.readLine()) != null)
            sql_schema += line + " ";

        Properties configurations = new Properties();
        configurations.load(new FileInputStream(new File(propertiesFile)));

        String sqlJDBC = configurations.getProperty("TVAR_SQL_JDBC");
        String sqlUser = configurations.getProperty("TVAR_SQL_USER");
        String sqlPassword = configurations.getProperty("TVAR_SQL_PASSWORD");

        String sql_db_name = configurations.getProperty("TVAR_SQL_DB_NAME");

        Statement statement = DriverManager.getConnection(sqlJDBC, sqlUser, sqlPassword)
                .createStatement();

        statement.execute("CREATE DATABASE IF NOT EXISTS " + sql_db_name);
        //Executing the Create Table
        statement.execute(sql_schema.replaceFirst("DB_NAME", sql_db_name));
    }
}
