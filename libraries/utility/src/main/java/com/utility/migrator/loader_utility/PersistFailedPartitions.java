package com.utility.migrator.loader_utility;

import com.utility.migrator.initializer.SQLUtility;
import com.utility.migrator.xml_parser.ConfigurationXMLParser;
import org.apache.commons.cli.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;

public class PersistFailedPartitions {
    public static void main(String[] args) throws Exception {

        ConfigurationXMLParser configurationXMLParser = new ConfigurationXMLParser();

        Options options = new Options();

        Option flow_name = new Option("f", "flow_name", true, "flow name");
        Option table_name = new Option("t", "sync_table_name", true, "sync table name");
        Option partition_details = new Option("p", "detailed_partitions", true, "partition details");
        Option config_file_path = new Option("c", "config_file_path", true, "config file path");

        flow_name.setRequired(true);
        table_name.setRequired(true);
        partition_details.setRequired(true);
        config_file_path.setRequired(true);

        options.addOption(flow_name);
        options.addOption(table_name);
        options.addOption(partition_details);
        options.addOption(config_file_path);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
            return;
        }

        String flowName = cmd.getOptionValue("flow_name");
        String tableName = cmd.getOptionValue("sync_table_name");
        String partitionDetails = cmd.getOptionValue("detailed_partitions");
        String confFilePath = cmd.getOptionValue("config_file_path");

        //Fetch the projectName, flowName, jobName, and tableName from the db_migration.ddh_persisted_flows
        ResultSet metadataDetails = new SQLUtility().getSQLConnection(confFilePath).createStatement()
                .executeQuery("SELECT project_id, flow_id, job_id FROM db_migration.job_dictionary WHERE sync_table='" + tableName + "' LIMIT 1");

        String ddhProjectName = "", ddhFlowName = "", ddhJobName = "";
        while (metadataDetails.next()){
            ddhProjectName = metadataDetails.getString(1);
            ddhFlowName = metadataDetails.getString(2);
            ddhJobName = metadataDetails.getString(3);
        }


        SQLUtility sqlUtility = new SQLUtility();
        String query = "INSERT INTO " + configurationXMLParser.getXMLProperty("clusters.mysql.db.name", confFilePath).concat(".")
                + configurationXMLParser.getXMLProperty("clusters.mysql.failed.load.table.name", confFilePath)
                + " (project_name, flow_name, job_name, table_name, partition_details, loader_timestamp) values (?, ?, ?, ?, ?, ?)";

        PreparedStatement preparedStmt = sqlUtility.getSQLConnection(confFilePath).prepareStatement(query);

        int itr=0;
        preparedStmt.setString (++itr, ddhProjectName);
        preparedStmt.setString (++itr, ddhFlowName);
        preparedStmt.setString (++itr, ddhJobName);
        preparedStmt.setString (++itr, tableName);
        preparedStmt.setString (++itr, partitionDetails);
        preparedStmt.setString (++itr, DateTimeFormat.forPattern("yyyy-MM-dd-HH:mm:ss").print(new DateTime(Calendar.getInstance())));
        boolean status = preparedStmt.execute();

        if (!status)
            System.out.println("Partition details successfully persisted - " + partitionDetails);
        else
            System.out.println("Failed to persist the partitions");
    }
}
