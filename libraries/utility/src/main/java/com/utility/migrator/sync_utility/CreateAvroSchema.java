package com.utility.migrator.sync_utility;

import com.utility.migrator.shema_validator.BigQueryUtilities;
import com.utility.migrator.shema_validator.HiveUtilities;
import com.utility.migrator.xml_parser.ConfigurationXMLParser;
import org.apache.commons.cli.*;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.*;
import java.util.*;

public class CreateAvroSchema {
    private String driverName = "org.apache.hive.jdbc.HiveDriver";

    public static void main (String args[]) throws Exception {

        /**
         * args[0] = Table Name at the destination
         * args[1] = ConfFilePath
         */
        Options options = new Options();

        Option table_name = new Option("t", "sync_table_name", true, "sync table name");
        Option config_file_path = new Option("c", "config_file_path", true, "config file path");

        table_name.setRequired(true);
        config_file_path.setRequired(true);

        options.addOption(table_name);
        options.addOption(config_file_path);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
            return;
        }

        String destinationTable = cmd.getOptionValue("sync_table_name");
        String confFilePath = cmd.getOptionValue("config_file_path");

        String avroSchema = new CreateAvroSchema().avroSchema(destinationTable, confFilePath);
        System.out.println(avroSchema);
    }

    public String avroSchema (String tableName, String confFilePath) throws Exception {

        ArrayList<String> table_columns, table_datatypes;

        Map<String, ArrayList<String>> column_details_dest =  new CreateAvroSchema().hiveSchemaDetails("destination", "describe " + tableName,
                confFilePath);

        table_columns = column_details_dest.get("columns");
        table_datatypes = column_details_dest.get("data_types");

        Iterator column_iterator = table_columns.iterator();
        Iterator datatypes_iterator = table_datatypes.iterator();

        JSONArray fields = new JSONArray();
        while (column_iterator.hasNext() && datatypes_iterator.hasNext()) {
            JSONObject columnsObject = new JSONObject();

            String columnType = new CreateAvroSchema().AvroDataTypeMapper(datatypes_iterator.next().toString());
            String columnName = column_iterator.next().toString();

            if ((columnType.toLowerCase().contains("map") || columnType.toLowerCase().contains("array")) && !columnType.toLowerCase().contains("array<struct")) {
                columnsObject.put ("name", columnName);
                JSONObject nestedMap = new JSONObject();

                if (columnType.contains("array")) {
                    nestedMap.put("type", "array");
                    ArrayList<String> multipleTypes = new ArrayList<>();
                    multipleTypes.add ("null");
                    multipleTypes.add (new CreateAvroSchema().AvroDataTypeMapper(columnType.substring(columnType.indexOf("<")+1, columnType.length()-1)));
                    nestedMap.put("items",multipleTypes);
                }
                else {
                    nestedMap.put("type", "map");
                    ArrayList<String> multipleTypes = new ArrayList<>();
                    multipleTypes.add ("null");
                    multipleTypes.add (new CreateAvroSchema().AvroDataTypeMapper(columnType.substring(columnType.indexOf("<")+1, columnType.length()-1).split(",")[1]));
                    nestedMap.put("values", multipleTypes);
                }
                JSONArray typeArray = new JSONArray();
                typeArray.put("null");
                typeArray.put(nestedMap);
                columnsObject.put("type", typeArray);
            }
            else if (columnType.toLowerCase().contains("struct") && !columnType.toLowerCase().contains("array<struct")) {
                JSONObject typesNested = new JSONObject();
                JSONArray fieldsNested = new JSONArray();

                typesNested.put ("type", "record");
                typesNested.put ("name", columnName);
                String columnDefinitions[] = new CreateAvroSchema().customSplitter(columnType.substring( columnType.indexOf("<")+1, columnType.length()-1 )).split(",");

                for ( int itr = 0; itr < columnDefinitions.length; ++itr ) {
                    JSONObject columnsObjectStruct = new JSONObject();

                    columnsObjectStruct.put ("name", columnDefinitions[itr].split(":")[0]);
                    ArrayList<String> multipleTypes = new ArrayList<>();
                    multipleTypes.add ("null");
                    multipleTypes.add (new CreateAvroSchema().AvroDataTypeMapper(columnDefinitions[itr].split(":")[1]));
                    columnsObjectStruct.put ("type", multipleTypes);
                    fieldsNested.put(columnsObjectStruct);
                }
                typesNested.put ("fields", fieldsNested);

                columnsObject.put ("name", columnName);
                JSONArray typeArray = new JSONArray();
                typeArray.put("null");
                typeArray.put(typesNested);
                columnsObject.put ("type", typeArray);
            }
            else if ( columnType.toLowerCase().contains("array<struct") ) {

                JSONObject typesNested = new JSONObject();
                JSONArray fieldsNested = new JSONArray();

                typesNested.put ("type", "record");
                typesNested.put ("name", columnName);
                String columnDefinitions[] = new CreateAvroSchema().customSplitter(columnType.substring(columnType.indexOf("array<struct<")+13, columnType.length()-2 )).split(",");

                for ( int itr = 0; itr < columnDefinitions.length; ++itr ) {
                    JSONObject columnsObjectStruct = new JSONObject();

                    columnsObjectStruct.put ("name", columnDefinitions[itr].split(":")[0]);
                    ArrayList<String> multipleTypes = new ArrayList<>();
                    multipleTypes.add ("null");
                    multipleTypes.add (new CreateAvroSchema().AvroDataTypeMapper(columnDefinitions[itr].split(":")[1]));
                    columnsObjectStruct.put ("type", multipleTypes);
                    fieldsNested.put(columnsObjectStruct);
                }
                typesNested.put ("fields", fieldsNested);
                typesNested.put ("namespace", "com.treselle.db.model");

                JSONObject typeForArrayStruct = new JSONObject();

                typeForArrayStruct.put ("type", "array");
                typeForArrayStruct.put ("items", typesNested);


                JSONArray typeArray = new JSONArray();
                typeArray.put("null");
                typeArray.put(typeForArrayStruct);
                columnsObject.put ("name", columnName);
                columnsObject.put ("type", typeArray);
            }
            else {
                columnsObject.put("name", columnName);
                ArrayList<String> multipleTypes = new ArrayList<>();
                multipleTypes.add ("null");
                multipleTypes.add (new CreateAvroSchema().AvroDataTypeMapper(columnType));
                columnsObject.put("type", multipleTypes);
            }
            fields.put(columnsObject);
        }

        JSONObject finalSchemaDefinition = new JSONObject();
        finalSchemaDefinition.put ("namespace", "Migrator_Utility_Schema");
        finalSchemaDefinition.put ("name", tableName);
        finalSchemaDefinition.put ("type", "record");
        finalSchemaDefinition.put ("fields", fields);

        return finalSchemaDefinition.toString();
    }

    public String AvroDataTypeMapper (String dataType) {

        if (dataType.equals("bigint"))
            return "long";
        else if (dataType.equals("binary"))
            return "bytes";
        else if (dataType.equals("tinyint"))
            return "int";
        else if (dataType.equals("smallint"))
            return "int";
        else if (dataType.contains("decimal") && !dataType.contains("struct"))
            return "double";
        else if (dataType.contains("char") && !dataType.contains("struct"))
            return "string";
        else if (dataType.contains("date") && !dataType.contains("struct"))
            return "string";
        else if (dataType.contains("timestamp") && !dataType.contains("struct"))
            return "string";
        else if (dataType.contains("varchar") && !dataType.contains("struct"))
            return "varchar";
        else
            return dataType;
    }

    public Map<String,ArrayList<String>> hiveSchemaDetails(String tableType, String query, String confFilePath) throws Exception {

        String hiveUrl="",hiveUser="",hivePassword="";

        if (tableType.equals("source")){
            hiveUrl = new ConfigurationXMLParser().getXMLProperty("clusters.src.hive.jdbc", confFilePath);
            hiveUser = new ConfigurationXMLParser().getXMLProperty("clusters.src.hive.user", confFilePath);
            hivePassword = new ConfigurationXMLParser().getXMLProperty("clusters.src.hive.password", confFilePath);
        }
        else if (tableType.equals("destination")) {
            hiveUrl = new ConfigurationXMLParser().getXMLProperty("clusters.dest.hive.jdbc", confFilePath);
            hiveUser = new ConfigurationXMLParser().getXMLProperty("clusters.dest.hive.user", confFilePath);
            hivePassword = new ConfigurationXMLParser().getXMLProperty("clusters.dest.hive.password", confFilePath);
        }
        else {
            System.out.println("None of the configuration values caught for Hive JDBC, Hive User and Hive Password");
            System.exit(1);
        }

        Map<String,ArrayList<String>> ret_values = new HashMap();
        try {
            Class.forName(driverName);
            Connection con = DriverManager.getConnection(hiveUrl, hiveUser, hivePassword);
            Statement stmt = con.createStatement();

            ArrayList<String> columns = new ArrayList<>();
            ArrayList<String> datatypes = new ArrayList<>();

            ResultSet rs = stmt.executeQuery(query);

            ResultSetMetaData metaData = rs.getMetaData();

            //To get the total column count
            int columnCount = metaData.getColumnCount();

            while (rs.next()) {
                if(rs.getString(metaData.getColumnName(2)) == null) {
                    break;
                }
                columns.add(rs.getString(metaData.getColumnName(1)));
                datatypes.add(rs.getString(metaData.getColumnName(2)));
            }
            ret_values.put("columns", columns);
            ret_values.put("data_types", datatypes);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (Exception ex) {
            System.out.println("Some form of error has occurred .....");
            ex.printStackTrace();
        }
        return ret_values;
    }

    public String customSplitter(String pattern) {

        String[] wordsInPattern = pattern.split(":");
        String finalString = "";
        for (int itr = 0; itr < wordsInPattern.length; ++itr) {
            if (wordsInPattern[itr].contains("decimal") && itr < wordsInPattern.length-1)
                finalString += "decimal," + wordsInPattern[itr].substring(wordsInPattern[itr].indexOf("),") + 2, wordsInPattern[itr].length()) + ":";
            else if (wordsInPattern[itr].contains("decimal") && itr == wordsInPattern.length-1)
                finalString += "decimal:";
            else if ((wordsInPattern[itr].contains("char") || wordsInPattern[itr].contains("varchar"))  && itr < wordsInPattern.length-1)
                finalString += "string," + wordsInPattern[itr].substring(wordsInPattern[itr].indexOf("),") + 2, wordsInPattern[itr].length()) + ":";
            else if ((wordsInPattern[itr].contains("char") || wordsInPattern[itr].contains("varchar")) && itr == wordsInPattern.length-1)
                finalString += "string:";
            else
                finalString += wordsInPattern[itr]+":";
        }

        finalString = finalString.substring(0, finalString.length()-1);
        return finalString;
    }
}
