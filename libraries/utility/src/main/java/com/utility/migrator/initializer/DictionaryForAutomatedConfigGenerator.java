package com.utility.migrator.initializer;

import java.io.File;
import java.io.FileInputStream;
import java.sql.*;
import java.util.HashMap;
import java.util.Properties;

public class DictionaryForAutomatedConfigGenerator {

    public String getGsBasePath (String ddhTableName, String propertyFilePath) throws Exception {

        ResultSet gsResultSet = new DictionaryForAutomatedConfigGenerator().getSQLConnection(propertyFilePath)
                .executeQuery("select gs_base_path from " + new DictionaryForAutomatedConfigGenerator()
                        .getPropertyValue("TVAR_SQL_FLOW_DICTIONARY_NAME", propertyFilePath)
                + " WHERE ddh_table_name='" + ddhTableName + "'");

        while (gsResultSet.next())
            return gsResultSet.getString(1);

        return null;
    }

    public String getBQSchemaName (String ddhTableName, String propertyFilePath) throws Exception {

        ResultSet bqResultSet = new DictionaryForAutomatedConfigGenerator().getSQLConnection(propertyFilePath)
                .executeQuery("select bq_schema_name from " + new DictionaryForAutomatedConfigGenerator()
                        .getPropertyValue("TVAR_SQL_FLOW_DICTIONARY_NAME", propertyFilePath)
                + " WHERE ddh_table_name='" + ddhTableName + "'");

        while (bqResultSet.next())
            return bqResultSet.getString(1);

        return null;
    }

    public String getS3BucketName (String ddhTableName, String propertyFilePath) throws Exception {

        ResultSet s3ResultSet = new DictionaryForAutomatedConfigGenerator().getSQLConnection(propertyFilePath)
                .executeQuery("select s3_bucket_name from " + new DictionaryForAutomatedConfigGenerator()
                        .getPropertyValue("TVAR_SQL_FLOW_DICTIONARY_NAME", propertyFilePath)
                + " WHERE ddh_table_name='" + ddhTableName + "'");

        while (s3ResultSet.next())
            return s3ResultSet.getString(1);

        return null;
    }

    public String getPropertyValue (String propertyName, String propertyFilePath) throws Exception {
        Properties configurations = new Properties();
        configurations.load(new FileInputStream(new File(propertyFilePath)));

        return configurations.getProperty(propertyName);
    }

    public Statement getSQLConnection (String propertyFilePath) throws Exception {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        try {
            String jdbcUrl = new DictionaryForAutomatedConfigGenerator().getPropertyValue("TVAR_SQL_JDBC", propertyFilePath);
            String sqlUser = new DictionaryForAutomatedConfigGenerator().getPropertyValue("TVAR_SQL_USER", propertyFilePath);
            String sqlPassword = new DictionaryForAutomatedConfigGenerator().getPropertyValue("TVAR_SQL_PASSWORD", propertyFilePath);
            Connection connection = DriverManager.getConnection(jdbcUrl, sqlUser, sqlPassword);
            return connection.createStatement();
        } catch (SQLException sqle){
            sqle.printStackTrace();
            return null;
        }
    }
}
