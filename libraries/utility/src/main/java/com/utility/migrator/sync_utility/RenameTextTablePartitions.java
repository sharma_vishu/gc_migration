package com.utility.migrator.sync_utility;

import com.utility.migrator.initializer.SQLUtility;
import com.utility.migrator.shema_validator.HiveUtilities;
import com.utility.migrator.xml_parser.ConfigurationXMLParser;
import org.apache.commons.cli.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.util.Calendar;

public class RenameTextTablePartitions {
    static org.slf4j.Logger LOG = LoggerFactory.getLogger(RenameTextTablePartitions.class);
    public static void main(String[] args) throws Exception {

        /**
         * args[0] - Flow Name
         * args[1] - Sync Table Name
         * args[2] - Config File Path
         */

        Options options = new Options();

        Option flow_name = new Option("f", "flow_name", true, "flow name");
        Option table_name = new Option("t", "sync_table_name", true, "sync table name");
        Option config_file_path = new Option("c", "config_file_path", true, "config file path");

        flow_name.setRequired(true);
        table_name.setRequired(true);
        config_file_path.setRequired(true);

        options.addOption(flow_name);
        options.addOption(table_name);
        options.addOption(config_file_path);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
            return;
        }

        String flowName = cmd.getOptionValue("flow_name");
        String tableName = cmd.getOptionValue("sync_table_name");
        String configFilePath = cmd.getOptionValue("config_file_path");

        String sqlDbName = new ConfigurationXMLParser().getXMLProperty("clusters.mysql.db.name", configFilePath);
        String sqlTableName = new ConfigurationXMLParser().getXMLProperty("clusters.mysql.flow.table.name", configFilePath);
        String mySqlTableName = sqlDbName.concat(".").concat(sqlTableName);

        String checkFlowExistsString = "select count(*) from " + mySqlTableName + " where flow_name='" + flowName +
                "' and table_name='" + tableName + "' and status='SUCCESSFUL'";
        System.out.println(checkFlowExistsString);
        int count = new SQLUtility().getCount(checkFlowExistsString, configFilePath);

        if (count == 0) {
            System.out.println("Batch Update : No need of Partition table renaming.....");
            System.out.println("Exiting from here ....");
        }
        else {

            String textTableName = tableName.concat("_text");
            String textTableLocation = new ConfigurationXMLParser()
                    .getXMLProperty("airbnb.reair.clusters.dest.hdfs.root", configFilePath)
                    .concat("_txt");

            String partitionType = textTableLocation.substring(0, textTableLocation.indexOf("://") + 3);
            //To Repair Table Name as often the metadata details are lost while doing BQ Load
            new HiveUtilities().HiveRunner("destination", "MSCK REPAIR TABLE " + tableName, configFilePath);

            ResultSet allParitions = new HiveUtilities().getHiveConnection("destination", configFilePath)
                    .executeQuery("show partitions " + textTableName);

            while (allParitions.next()) {
                Runtime rt = Runtime.getRuntime();
                String completePartitions = textTableLocation.concat("/" + allParitions.getString(1));
                System.out.println("Complete Partitions = " + completePartitions);
                Process proc = rt.exec("hadoop fs -ls " + completePartitions);

                BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
                String partition;
                while ((partition = in.readLine()) != null) {
                    if (partition.indexOf(partitionType) == -1)
                        continue;

                    System.out.println(partition);
                    String timeStamp = DateTimeFormat.forPattern("yyyyMMdd_HHmmss").print(new DateTime(Calendar.getInstance()));
                    String filePath = partition.substring(partition.indexOf(partitionType), partition.length());
                    String newFilePath = filePath.concat("_" + timeStamp);
                    System.out.println("FilePath = " + filePath + "\t New File Path = " + newFilePath);
                    Process renameProcess = rt.exec("hadoop fs -mv " + filePath + " " + newFilePath);
                    renameProcess.waitFor();
                    new RenameTextTablePartitions().ErrorStream(renameProcess);
                }
                in.close();

                proc.waitFor();
                LOG.info("Exit Value = " + proc.exitValue());
            }

            new HiveUtilities().HiveRunner("destination", "MSCK REPAIR TABLE " + tableName, configFilePath);
        }
    }

    public void ErrorStream (Process proc) throws Exception {
        System.out.println("Printing the Input Strean ....");
        BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
        String op;
        while ((op = in.readLine()) != null) {
            System.out.println(op);
        }
        in.close();
        System.out.println("Printing the Error Strean ....");
        BufferedReader in1 = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
        while ((op = in1.readLine()) != null) {
            System.out.println(op);
        }
        in1.close();
    }
}