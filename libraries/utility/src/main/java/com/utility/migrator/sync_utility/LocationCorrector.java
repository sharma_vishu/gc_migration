package com.utility.migrator.sync_utility;

import com.utility.migrator.shema_validator.HiveUtilities;
import com.utility.migrator.xml_parser.ConfigurationXMLParser;
import org.apache.commons.cli.*;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Logger;

public class LocationCorrector {
    private static String driverName = "org.apache.hive.jdbc.HiveDriver";
    public static void main(String[] args) throws Exception {

        /*
            args[0] : Table Name
            args[1] : Configuration file Path
         */
        Options options = new Options();

        Option table_name = new Option("t", "sync_table_name", true, "sync table name");
        Option config_file_path = new Option("c", "config_file_path", true, "config file path");

        table_name.setRequired(true);
        config_file_path.setRequired(true);

        options.addOption(table_name);
        options.addOption(config_file_path);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
            return;
        }

        String destinationTableName = cmd.getOptionValue("sync_table_name");
        String confFilePath = cmd.getOptionValue("config_file_path");

        Class.forName(driverName);

        String orcTableExists = new LocationCorrector().checkIfTableisORC("destination", destinationTableName, confFilePath);

        if( orcTableExists.equals("false") ) {

            String tableLocationCorrector = "ALTER TABLE " + destinationTableName + " SET LOCATION '" + new ConfigurationXMLParser()
                    .getXMLProperty("airbnb.reair.clusters.dest.hdfs.root", confFilePath) + "'";

            new HiveUtilities().HiveRunner("destination", tableLocationCorrector, confFilePath);

            /*
            Updating the table Partitions at the destination
            */
            new HiveUtilities().HiveRunner("destination", "MSCK REPAIR TABLE " + destinationTableName, confFilePath);
            System.out.println("Table " + destinationTableName + " partitions successfully updated ...");

        }
        else {


            /*
            recreating ORC main Table
             */
                String srcTableSchema = new LocationCorrector().getTableSchema("source", destinationTableName, confFilePath);
                srcTableSchema += " STORED AS ORC \n";
                srcTableSchema += " LOCATION '" + new ConfigurationXMLParser().getXMLProperty("airbnb.reair.clusters.dest.hdfs.root", confFilePath) + "'";

                String filteredTableString = "CREATE EXTERNAL".concat(
                        srcTableSchema.substring(srcTableSchema.toLowerCase().indexOf("external") + 8, srcTableSchema.length()));

                System.out.println("New Table Schema = " + filteredTableString);
                //Re-Configuring hive runner table in the destination Hive for ORC Formats
            new HiveUtilities().HiveRunner("destination", "DROP TABLE IF EXISTS " + destinationTableName, confFilePath);
            new HiveUtilities().HiveRunner("destination", filteredTableString, confFilePath);

            //Updating the table Partitions at the destination
            new HiveUtilities().HiveRunner("destination", "MSCK REPAIR TABLE " + destinationTableName, confFilePath);
            System.out.println("Table " + destinationTableName + " partitions successfully updated ...");

        }

        System.out.println("Location Corrector Stage Updated Successfully ......");
    }

    public String getTableSchema(String tableType, String hiveTblName, String confFilePath) throws Exception {
        Class.forName(driverName);
        Statement st = new HiveUtilities().getHiveConnection(tableType, confFilePath);
        System.out.println("show create table " + hiveTblName);
        ResultSet rs = st.executeQuery("show create table "+ hiveTblName);

        String tableDef="";
        while (rs.next()) {
            String item = rs.getString(1);

            if ((item.contains("ROW FORMAT SERDE") || item.contains("INPUTFORMAT") || item.contains("OUTPUTFORMAT") || item.contains("LOCATION"))
                    && !item.contains("COMMENT '") )
                break;
            else
                tableDef +=  rs.getString(1) + " ";
        }

        return tableDef;
    }

    public String checkIfTableisORC (String tableType, String hiveTblName, String confFilePath) throws Exception {
        Class.forName(driverName);
        Statement st = new HiveUtilities().getHiveConnection(tableType, confFilePath);
        System.out.println("show create table " + hiveTblName);
        ResultSet rs = st.executeQuery("show create table "+ hiveTblName);

        while (rs.next()) {
            String item = rs.getString(1);
            if (item.contains("OrcInputFormat") || item.contains("OrcOutputFormat"))
                return "true";
            else
                continue;
        }

        return "false";
    }
}
