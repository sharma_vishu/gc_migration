package com.utility.migrator.xml_parser;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

public class ConfigurationXMLParser {
    public String getXMLProperty(String propertyName, String fileName) throws Exception {

        Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File(fileName));

        document.getDocumentElement().normalize();

        NodeList nList = document.getElementsByTagName("property");

        String propertyValue ="";

        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                if (!eElement.getElementsByTagName("name").item(0).getTextContent().equals(propertyName))
                    continue;

                propertyValue = eElement.getElementsByTagName("value").item(0).getTextContent();
            }
        }

        return propertyValue;
    }
}
