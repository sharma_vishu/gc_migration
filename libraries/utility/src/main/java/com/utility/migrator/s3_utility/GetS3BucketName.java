package com.utility.migrator.s3_utility;
;
import com.utility.migrator.shema_validator.SqlUtilities;
import com.utility.migrator.xml_parser.ConfigurationXMLParser;
import org.apache.commons.cli.*;

import java.sql.ResultSet;

public class GetS3BucketName {
    public static void main(String[] args) {
        try {
            Options options = new Options();

            Option table_name = new Option("t", "sync_table_name", true, "sync table name");
            Option config_file_path = new Option("c", "config_file_path", true, "config file path");

            table_name.setRequired(true);
            config_file_path.setRequired(true);

            options.addOption(table_name);
            options.addOption(config_file_path);

            CommandLineParser parser = new DefaultParser();
            HelpFormatter formatter = new HelpFormatter();
            CommandLine cmd;

            try {
                cmd = parser.parse(options, args);
            } catch (ParseException e) {
                System.out.println(e.getMessage());
                formatter.printHelp("utility-name", options);

                System.exit(1);
                return;
            }

            String tableName = cmd.getOptionValue("sync_table_name");
            String confFilePath = cmd.getOptionValue("config_file_path");
            System.out.println(new GetS3BucketName().getS3BucketName(tableName, confFilePath));
        } catch (Exception ex ) {
            ex.printStackTrace();
        }
    }

    public String getS3BucketName (String tableName, String configFilePath) throws Exception {
        String mySqlDBName = new ConfigurationXMLParser().getXMLProperty("clusters.mysql.db.name", configFilePath);
        String mySqlTblName = new ConfigurationXMLParser().getXMLProperty("clusters.mysql.dictionary.table.name", configFilePath);
        String mySqlTableName = mySqlDBName.concat("." + mySqlTblName);

        ResultSet s3ResultSet = new SqlUtilities()
                .getSQLConnection(configFilePath)
                .createStatement()
                .executeQuery("SELECT s3_bucket_name FROM " + mySqlTableName + " WHERE ddh_table_name='" + tableName + "'");

        while (s3ResultSet.next())
            return s3ResultSet.getString(1);

        return null;
    }
}
