package com.utility.migrator.shema_validator;

import com.utility.migrator.xml_parser.ConfigurationXMLParser;

import java.sql.*;

public class SqlUtilities {
    private ConfigurationXMLParser configurationXMLParser = new ConfigurationXMLParser();

    public Connection getSQLConnection(String confFilePath) throws Exception {
        try {
            /*//TODO : For Cloud SQL
            System.out.println("SQL Table Name = " + sqlTableName);
            int indexedTable = sqlTableName.indexOf('.');

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String jdbcUrl = String.format(
                    "jdbc:mysql://google/%s?cloudSqlInstance=%s&"
                            + "socketFactory=com.google.cloud.sql.mysql.SocketFactory",
                    sqlTableName.substring(0, indexedTable),
                    instanceConnectionName);

            Connection connection = DriverManager.getConnection(jdbcUrl, user, password);
            return connection.createStatement();*/

            //TODO : For MSQL Connection
            String jdbcUrl = configurationXMLParser.getXMLProperty("clusters.mysql.host", confFilePath);
            String sqlUser = configurationXMLParser.getXMLProperty("clusters.mysql.user", confFilePath);
            String sqlPassword = configurationXMLParser.getXMLProperty("clusters.mysql.password", confFilePath);

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection connection = DriverManager.getConnection(jdbcUrl, sqlUser, sqlPassword);
            return connection;

        } catch (SQLException sqle){
            sqle.printStackTrace();
            return null;
        }
    }

    public boolean updatePersistantSchema(String hiveTableName, String ddhSchema, String dataProcSchema,
                                                 String bigQuerySchema, String confFilePath) throws Exception {

        String sqlDbName = configurationXMLParser.getXMLProperty("clusters.mysql.db.name", confFilePath);
        String sqlTableName = configurationXMLParser.getXMLProperty("clusters.mysql.schema.table.name", confFilePath);
        String mySqlTable = sqlDbName + "." + sqlTableName;
        Statement st = getSQLConnection(confFilePath).createStatement();

        String updateTable = "UPDATE " + mySqlTable + " SET ddh_schema=\""+ddhSchema+"\", dataproc_schema=\""
                +dataProcSchema+"\", bigquery_schema='"+bigQuerySchema+"' WHERE table_name=\""+hiveTableName+"\"";

        System.out.println(updateTable);

        Boolean result = st.execute(updateTable);
        System.out.println(result);
        if (!result) {
            System.out.println("Schema Update failed ....");
        }
        else
            System.out.println("Schema update successful ....");

        return result;
    }

    public boolean persistantStorage(String tableName, String ddhSchema, String dataProcSchema, String bigQuery,
                                            String confFilePath) throws Exception {

        String sqlDbName = configurationXMLParser.getXMLProperty("clusters.mysql.db.name", confFilePath);
        String sqlTableName = configurationXMLParser.getXMLProperty("clusters.mysql.schema.table.name", confFilePath);
        Statement stmt = getSQLConnection(confFilePath).createStatement();

        String mySqlTableName = sqlDbName.concat(".").concat(sqlTableName);

        stmt.execute("CREATE DATABASE IF NOT EXISTS "+ sqlDbName);
        stmt.execute("CREATE TABLE IF NOT EXISTS "+ mySqlTableName +" (table_name varchar(300), ddh_schema varchar(2000), dataproc_schema varchar(2000), bigquery_schema varchar(2000) );");

        String values = "\""+tableName+"\",\""+ddhSchema+"\",\""+dataProcSchema+"\",\'"+bigQuery+"\'";

        String sql = "INSERT INTO "+ mySqlTableName +" (table_name,ddh_schema,dataproc_schema,bigquery_schema) VALUES ("+
                values + ")";

        Boolean outcome = stmt.execute(sql);
        return outcome;
    }

    public boolean checkSQLTableExists(String recordTableName, String confFilePath) throws Exception {

        String mySqlDbName = configurationXMLParser.getXMLProperty("clusters.mysql.db.name", confFilePath);
        String mySqlFlowTableName = configurationXMLParser.getXMLProperty("clusters.mysql.flow.table.name", confFilePath);

        String mySqlTableName = mySqlDbName.concat(".").concat(mySqlFlowTableName);

        Statement stmt = getSQLConnection(confFilePath).createStatement();
        String checkTableQuery = "select count(*) from "+ mySqlTableName +" where table_name=\""+recordTableName+"\"" +
                " and (status != 'RUNNING' OR job_stage NOT IN ('Stage 1 : Job Initialization'))";
        System.out.println(checkTableQuery);
        ResultSet rs = stmt.executeQuery(checkTableQuery);

        int count=0;
        while(rs.next()) {
            count = rs.getInt(1);
        }

        if (count == 0)
            return false;
        else
            return true;
    }
}
