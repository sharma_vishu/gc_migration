package com.trigger.utilities;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import org.apache.http.client.HttpClient;

import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;

import org.apache.http.conn.ssl.*;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.Properties;

public class AzkabanJobTrigger {
    public static final Logger LOG = Logger.getLogger(AzkabanJobTrigger.class);

    public long executeAzkabanJob (String syncTableName, String configFilePath) throws Exception {

        Properties properties = new Properties();
        properties.load(new FileInputStream(configFilePath));

        String secureURL = properties.getProperty("AZKABAN_SECURE_URL");
        String azkabanHost = properties.getProperty("AZKABAN_HOST_NAME");
        String azkabanPort = properties.getProperty("AZKABAN_HOST_PORT");
        String azkabanProjectId = properties.getProperty("AZKABAN_PROJECT_ID");
        String azkabanFlowId = properties.getProperty("AZKABAN_FLOW_ID");

        //Deciding whether to use Http or Https
        String azkabanURL;
        if (secureURL.equals("YES"))
            azkabanURL = "https://" + azkabanHost + ":" + azkabanPort;
        else
            azkabanURL = "http://" + azkabanHost + ":" + azkabanPort;

        Unirest.setHttpClient(init());
        // Get session
        HttpResponse<JsonNode> jsonResponse = Unirest.post(azkabanURL + "/?action=login&username="
                + properties.getProperty("AZKABAN_USER") + "&password=" + properties.getProperty("AZKABAN_PASSWORD"))
                .header("accept", "application/json").asJson();

        JSONObject sessionJsonObject = jsonResponse.getBody().getObject();
        String sessionId = sessionJsonObject.getString("session.id");
        System.out.println(sessionId);

        String submitJobFlow = Unirest.get(
                azkabanURL + "/executor?ajax=executeFlow&session.id=" + sessionId + "&project=" + azkabanProjectId
                        + "&flow=" + azkabanFlowId
                        + "&flowOverride[SYNC_TABLE]=" + syncTableName
                        + "&flowOverride[FLOW_NAME]=" + azkabanFlowId  )
                .header("accept", "application/json").asString().getBody();


        LOG.info("JSON RECEIVED FROM AZKABAN = " + submitJobFlow);
        JSONObject responseJson = new JSONObject(submitJobFlow);

        return Long.parseLong(responseJson.get("execid").toString());

    }

    private static HttpClient init() throws AzkabanClientException {
        SSLSocketFactory sslsf;
        try {
            sslsf = new SSLSocketFactory(new TrustStrategy() {
                public boolean isTrusted(final java.security.cert.X509Certificate[] chain, final String authType)
                        throws java.security.cert.CertificateException {
                    return true;
                }
            }, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            final Scheme https = new Scheme("https", 8443, sslsf);
            final Scheme http = new Scheme("http", 80, PlainSocketFactory.getSocketFactory());
            final HttpClient httpClient = new DefaultHttpClient();
            final SchemeRegistry schemeRegistry = httpClient.getConnectionManager().getSchemeRegistry();
            schemeRegistry.register(https);
            schemeRegistry.register(http);

            return httpClient;

        } catch (KeyManagementException e) {
            LOG.error(e.getMessage(), e);
            throw new AzkabanClientException(e.getMessage(), e);
        } catch (UnrecoverableKeyException e) {
            LOG.error(e.getMessage(), e);
            throw new AzkabanClientException(e.getMessage(), e);
        } catch (NoSuchAlgorithmException e) {
            LOG.error(e.getMessage(), e);
            throw new AzkabanClientException(e.getMessage(), e);
        } catch (KeyStoreException e) {
            LOG.error(e.getMessage(), e);
            throw new AzkabanClientException(e.getMessage(), e);
        }
    }
}
