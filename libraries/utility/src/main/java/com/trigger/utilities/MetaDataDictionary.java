package com.trigger.utilities;

import java.util.HashMap;
import java.util.Map;

public class MetaDataDictionary {
    static Map<Integer, String > statusDictionary = new HashMap<Integer, String>();

    static {
        statusDictionary.put(10, "READY");
        statusDictionary.put(20, "PREPARING");
        statusDictionary.put(30, "RUNNING");
        statusDictionary.put(40, "PAUSED");
        statusDictionary.put(50, "SUCCEEDED");
        statusDictionary.put(60, "KILLED");
        statusDictionary.put(70, "FAILED");
        statusDictionary.put(80, "FAILED_FINISHING");
        statusDictionary.put(90, "SKIPPED");
        statusDictionary.put(100, "DISABLED");
        statusDictionary.put(110, "QUEUED");
        statusDictionary.put(120, "FAILED_SUCCEEDED");
        statusDictionary.put(130, "CANCELLED");
    }
}
