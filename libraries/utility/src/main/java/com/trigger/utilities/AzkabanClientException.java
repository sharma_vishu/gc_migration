package com.trigger.utilities;

public class AzkabanClientException extends Exception {

    public AzkabanClientException(final String message, final Throwable e) {
        super(message, e);
    }

    public AzkabanClientException(final String message) {
        super(message);
    }

    private static final long serialVersionUID = 1L;
}
