package com.trigger.utilities;

import java.io.FileInputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Properties;

public class SQLUtilities {
    public String getSyncTableName (String projectId, String flowId, String jobId, String confFilePath) throws Exception {
        Properties configurations = new Properties();
        configurations.load(new FileInputStream(confFilePath));

        Statement fetchStatement = new SQLUtilities()
                .getSQLConnection("CLOUD_CLUSTER", confFilePath)
                .createStatement();
        String fetchTableTemplate = "SELECT sync_table FROM CLOUD_DICT_TBL_NAME WHERE project_id = 'PROJECT_ID' " +
                "and flow_id = 'FLOW_ID' and job_id = 'JOB_ID';";
        String fetchTable = fetchTableTemplate
                .replace ("PROJECT_ID", projectId)
                .replace ("FLOW_ID", flowId)
                .replace ("JOB_ID", jobId)
                .replace("CLOUD_DICT_TBL_NAME", configurations.getProperty("CLOUD_DICT_TBL_NAME"));

        ResultSet fetchTableResultSet = fetchStatement.executeQuery(fetchTable);
        String sync_table="";
        while (fetchTableResultSet.next()){
            sync_table = fetchTableResultSet.getString(1);
        }
        fetchStatement.close();
        return sync_table;
    }

    public int isTheJobMonitored (String projectId, String flowId, String jobId, String confFilePath) throws Exception {

        Properties configurations = new Properties();
        configurations.load(new FileInputStream(confFilePath));

        Statement monitoringStatement = new SQLUtilities()
                .getSQLConnection("CLOUD_CLUSTER", confFilePath)
                .createStatement();
        String countQueryTemplate = "SELECT count(*) FROM CLOUD_DICT_TBL_NAME WHERE project_id = 'PROJECT_ID' " +
                "and flow_id = 'FLOW_ID' and job_id = 'JOB_ID';";

        String countQuery = countQueryTemplate
                .replace ("PROJECT_ID", projectId)
                .replace ("FLOW_ID", flowId)
                .replace ("JOB_ID", jobId)
                .replace("CLOUD_DICT_TBL_NAME", configurations.getProperty("CLOUD_DICT_TBL_NAME"));

        ResultSet monitoringResultSet = monitoringStatement.executeQuery(countQuery);
        int count = 0;
        while (monitoringResultSet.next()){
            count = monitoringResultSet.getInt(1);
        }
        monitoringStatement.close();
        return count;
    }

    public String getProjectName (int projectId, String confFilePath) throws Exception {
        System.out.println("projectId="+projectId);
        String query = "select name from azkaban.projects where id=" + projectId;
        Connection connection = new SQLUtilities()
                .getSQLConnection("DDH_CLUSTER", confFilePath);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        //TODO remove this default once it works
        String projectName="MyProj";

        if(resultSet.next()) {
            do {
                System.out.println("test");
                System.out.println("projectName=" + resultSet.getString(1));
                projectName = resultSet.getString(1);
            } while (resultSet.next());
        }
        else
            projectName = null;

        statement.close();
        connection.close();
        return projectName;
    }

    public Connection getSQLConnection(String type, String confFilePath) throws Exception {
        try {
            Properties configurations = new Properties();
            configurations.load(new FileInputStream(confFilePath));

            String sqlJDBC="", sqlUser="", sqlPassword="";
            if (type.equals("DDH_CLUSTER")){

                sqlJDBC = configurations.getProperty("DDH_JDBC");
                sqlUser = configurations.getProperty("DDH_USER");
                sqlPassword = configurations.getProperty("DDH_PASSWORD");

            } else if (type.equals("CLOUD_CLUSTER")) {

                sqlJDBC = configurations.getProperty("CLOUD_JDBC");
                sqlUser = configurations.getProperty("CLOUD_USER");
                sqlPassword = configurations.getProperty("CLOUD_PASSWORD");

            } else {
                System.out.println("No Known Cluster Arguments Detected");
                System.exit(1);
            }

            Class.forName("com.mysql.jdbc.Driver").newInstance();

            return DriverManager.getConnection(sqlJDBC, sqlUser, sqlPassword);

        } catch (SQLException sqle) {
            sqle.printStackTrace();
            return null;
        }
    }

    public ArrayList<String> getMonitoringList(String confFilePath) throws Exception {

        Properties configurations = new Properties();
        configurations.load(new FileInputStream(confFilePath));

        ArrayList<String> dictionaryList = new ArrayList();

        String cloudSQLTblName = configurations.getProperty("CLOUD_DICT_TBL_NAME");
        String query = "SELECT * FROM " + cloudSQLTblName;

        ResultSet resultSet = new SQLUtilities()
                .getSQLConnection("CLOUD_CLUSTER", confFilePath).createStatement().executeQuery(query);

        while (resultSet.next()) {
            String projectId = resultSet.getString(1);
            String flowId = resultSet.getString(2);

            String listString = projectId + "," + flowId;
            dictionaryList.add(listString);
        }
        resultSet.close();
        return dictionaryList;
    }
}
