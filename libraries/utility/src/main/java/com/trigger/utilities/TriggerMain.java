package com.trigger.utilities;

import org.apache.commons.cli.*;

import java.io.FileInputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

public class TriggerMain {
    public static void main(String[] args) throws Exception {

        Options options = new Options();

        Option config_file_path = new Option("c", "config_file_path", true, "config file path");
        config_file_path.setRequired(true);
        options.addOption(config_file_path);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
            return;
        }
        //Conf File Path
        String confFilePath = cmd.getOptionValue("config_file_path");

        Properties configurations = new Properties();
        configurations.load(new FileInputStream(confFilePath));
        //To trigger the jobs only once for any trigger run ....
        ArrayList<String> historicalTriggers = new ArrayList<>();

        /** Defining Offset **/
        String cloudSQLTblName = configurations.getProperty("PERSISTED_CLOUD_TABLE_NAME");
        String azkanbanExeJobTblName = configurations.getProperty("AZKABAN_EXE_JOBS_TBL");

        String offsetQuery = "SELECT max(end_time) FROM " + cloudSQLTblName;

        Statement offsetStatement = new SQLUtilities()
                .getSQLConnection("CLOUD_CLUSTER", confFilePath).createStatement();

        ResultSet offsetResultSet = offsetStatement.executeQuery(offsetQuery);

        long maxEndTime=-1;

        while (offsetResultSet.next()){
            maxEndTime = offsetResultSet.getLong(1);
        }

        if (maxEndTime == -1) {
            System.out.println("Unable to fetch the max end time from the persisted SQL, looks like first run...");
        }

        if (maxEndTime == -1)
            maxEndTime = Long.parseLong(configurations.getProperty("OFFSET_START"));
        System.out.println("maxEndTime = "+maxEndTime);

        String sqlQuery = "SELECT exec_id, project_id, flow_id, job_id, status, start_time, end_time, attempt " +
                "FROM " + azkanbanExeJobTblName + " where status=50 and end_time > " + maxEndTime + " ORDER BY exec_id DESC, end_time DESC";
//TODO remove it once testing is finish
        String checkCount = "SELECT count(*) " +
                "FROM " + azkanbanExeJobTblName + " where status=50 and end_time > " + maxEndTime;

        Statement statement3 = new SQLUtilities().getSQLConnection("DDH_CLUSTER",confFilePath).createStatement();
        ResultSet newresultSet = statement3.executeQuery(checkCount);
        boolean hasResult = newresultSet.first();
        System.out.println("count = "+newresultSet.getInt(1));


        Statement statement = new SQLUtilities().getSQLConnection("DDH_CLUSTER", confFilePath).createStatement();
        //Get all the monitoring jobs at once
        ArrayList<String> monitoredJobs = new SQLUtilities().getMonitoringList(confFilePath);
        String query = "INSERT INTO " + cloudSQLTblName + "(exec_id, project_id, flow_id, job_id, status, start_time, " +
                "end_time, attempt, marker)" + " values (?, ?, ?, ?, ?, ?, ?, ?,?)";
        PreparedStatement preparedStmt = new SQLUtilities().getSQLConnection("CLOUD_CLUSTER", confFilePath).prepareStatement(query);

        ResultSet resultSet = statement.executeQuery(sqlQuery);
        while (resultSet.next()){
            int exec_id = resultSet.getInt(1);
            System.out.println("exec_id="+exec_id);

            int project_id = resultSet.getInt(2) ;
            String projectName = new SQLUtilities().getProjectName(project_id, confFilePath);
            //String projectName ="Sync--Trigger-Test1";
                    System.out.println("projectName="+projectName);
            if(null == projectName) {
                continue;
            }
            String flow_id = resultSet.getString(3);
            String job_id = resultSet.getString(4);

            int status_int = resultSet.getInt(5) ;
            String status = MetaDataDictionary.statusDictionary.get(status_int);

            long start_time = resultSet.getLong(6);
            long end_time = resultSet.getLong(7);
            int attempt = resultSet.getInt(8) ;

            /*   ======         If not monitored Job, skip from here  ====== */
            String presentCompositeKey = projectName + "," + flow_id;

            if (!monitoredJobs.contains(presentCompositeKey)) {
                System.out.println("Parsed execution id - " + exec_id + " and skipped, as it isn't a part of the monitored flow !");
                continue;
            }
            /*   ====== ====================================================== ====== */
            System.out.println("Execution id - " + exec_id + " is a part of monitored job, necessary actions follow");
            if (historicalTriggers.contains(presentCompositeKey)){
                //This job has already been run sometimes in this flow, so no more trigger to follow ....
                continue;
            } else
                historicalTriggers.add(presentCompositeKey);

            //Persistance to MySQL Table
           // String query = "INSERT INTO " + cloudSQLTblName + "(exec_id, project_id, flow_id, job_id, status, start_time, " +
                   // "end_time, attempt, marker)" + " values (?, ?, ?, ?, ?, ?, ?, ?,?)";

          //  PreparedStatement preparedStmt = new SQLUtilities().getSQLConnection("CLOUD_CLUSTER", confFilePath).prepareStatement(query);

            int itr = 0;
            preparedStmt.setInt (++itr, exec_id);
            preparedStmt.setString (++itr, projectName);
            preparedStmt.setString (++itr, flow_id);
            preparedStmt.setString (++itr, job_id);
            preparedStmt.setString (++itr, status);
            preparedStmt.setLong (++itr, start_time);
            preparedStmt.setLong (++itr, end_time);
            preparedStmt.setInt (++itr, attempt);

            int count = new SQLUtilities().isTheJobMonitored (projectName, flow_id, job_id, confFilePath);

            long executionIdFromRest = -1;

            if (count == 1) {
                //Get the Source Table Name which needs to be triggered
                String syncTableName = new SQLUtilities()
                        .getSyncTableName(projectName, flow_id, job_id, confFilePath);
                if (syncTableName.length() == 0){
                    System.out.println("No matching Table in Source cluster for credentials " + projectName + "," +
                    flow_id + "," + job_id);
                    break;
                }

                //REST CALL TO TRIGGER THE AZKABAN
                executionIdFromRest = new AzkabanJobTrigger().executeAzkabanJob ( syncTableName, confFilePath );

                //Persisting the Execution Id
                preparedStmt.setString (++itr, "Successfully Triggered Metastore Framework Flow. " +
                        "Execution Id = " + executionIdFromRest);

                System.out.println(exec_id+"\t"+projectName+"\t"+flow_id+"\t"+job_id+"\t"+status+"\t"+start_time
                        +"\t"+end_time+"\t"+attempt);

                if ( !preparedStmt.execute() ) {
                    if ( executionIdFromRest !=-1 )
                        System.out.println("Job Details from Azkaban Cluster for Execution Id = " + executionIdFromRest + " submitted successfully");
                }
                else
                    System.out.println("Failed to persist Execution Id = " + exec_id);
            }
        }
        preparedStmt.close();
    }
}
